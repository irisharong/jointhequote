<div class="dashboard-container">
<?php 
	$classInventory = $menu == 'inventory' ? "class='active'" : "class=''";
	$classTeam = $menu == 'team' ? "class='active'" : "class=''";	
  $classSettings = $menu == 'settings' ? "class='active'" : "class=''";
  $classMessage = $menu == 'messages' ? "class='active'" : "class=''";
  $classRequest = $menu == 'request' ? "class='active'" : "class=''";

?>
	<!-- Dashboard Sidebar
	================================================== -->
	<div class="dashboard-sidebar">
		<div class="dashboard-sidebar-inner" data-simplebar>
			<div class="dashboard-nav-container">

				<!-- Responsive Navigation Trigger -->
				<a href="#" class="dashboard-responsive-nav-trigger">
					<span class="hamburger hamburger--collapse" >
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</span>
					<span class="trigger-title">Dashboard Navigation</span>
				</a>
				
				<!-- Navigation -->
				<div class="dashboard-nav">
					<div class="dashboard-nav-inner">

						<ul>
							<?php if($level == "dealer"){ ?>
							<li <?php echo $classTeam; ?>><a href="<?php echo base_url('dealer/team'); ?>"><i class="icon-material-outline-group"></i>My Team</a></li>
							<?php } ?>
							<li <?php echo $classRequest; ?>><a href="<?php echo base_url('dealer/request'); ?>"><i class="icon-material-outline-group"></i>Customer Requests</a></li>
						 
							<li <?php echo $classInventory; ?>><a href="<?php echo base_url('dealer/inventory'); ?>"><i class="icon-material-outline-star-border"></i>Inventory</a></li>
							<!--<li <?php echo $classMessage; ?>><a href="<?php echo base_url('dealer/messages'); ?>"><i class="icon-feather-message-square"></i>Messages</a></li>-->
							<li <?php echo $classSettings; ?>><a href="<?php echo base_url('dealer/settings'); ?>"><i class="icon-material-outline-assignment"></i> Settings</a></li>
						</ul>
						 
					</div>
				</div>
				<!-- Navigation / End -->

			</div>
		</div>
	</div>