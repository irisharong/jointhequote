<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use Twilio\Rest\Client;
class Customer extends CI_Controller {

	public function __construct(){
        parent::__construct();    
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->model('vehicle_model');
        $this->load->model('message_model');
        $this->load->model('register_model');
        $this->load->library("geozip");
        $this->login_model->is_logged_in();   
        $level = $this->session->userdata('level');
        
       
    }
    
    function index($requestId = false){ //dashboard
         
        $user = $this->user_model;
        $user_id = $this->session->userdata('user_id');
        
        $data['record'] = $user->get_all_users();
        if(!$data['record']){
            $data['nodata'] = TRUE;           
        }
        $data['title'] = 'Users';
        $data['main'] = 'users';
        $data['user_id'] = $user_id;

        $request = $this->vehicle_model->get_customer_request($user_id);   
        $dealership = $this->vehicle_model->get_all_dealership(); 
        $zipcode = "";
        $quote_result = array();
        $nomatch = array();
        $match = false;
   
        foreach($request as $req){   

            $data['firstname'] = $req->firstname; 
            $data['lastname'] = $req->lastname; 
            $data['email'] = $req->email; 
            $data['phone'] = $req->phone; 
 
            //get all dealer zipcode and compare the distance with the requested vehicle
            foreach($dealership as $zipcode){ 
                $distance = $this->geozip->get_distance($req->zip_code, $zipcode);
                
                if($distance !== false){ 
                    //check if distance match with request miles
                
                    if($distance <= $req->miles){  
                        $quote_result[$req->id] = $this->vehicle_model->get_quotes($req->id);
                        $match = true;
                    } 
                    else{
                        $quote_result[$req->id] =  false ;  
                        $nomatch[$req->id]['zipcode'] = ($req->zip_code == '') ? 0 : $req->zip_code;
                        $nomatch[$req->id]['miles'] = $req->miles;
                        $nomatch[$req->id]['requestId'] = $req->id; 
                    } 
                } 
                else{ 
                    
                    $quote_result[$req->id] =  false ; 
                    $nomatch[$req->id]['zipcode'] = ($req->zip_code == '') ? 0 : $req->zip_code;
                    $nomatch[$req->id]['miles'] = $req->miles;
                    $nomatch[$req->id]['requestId'] = $req->id; 
                }  
            }  
            if(empty($quote_result[$req->id]) && $match == false){
                $quote_result[$req->id] =  false ;
                $nomatch[$req->id]['zipcode'] = ($req->zip_code == '') ? 0 : $req->zip_code;
                $nomatch[$req->id]['miles'] = $req->miles;
                $nomatch[$req->id]['requestId'] = $req->id;
        
            }   
             
        }  
 
        $user_info = $this->user_model->user_info(); 
        $firstName = "";
        foreach($user_info as $user){
            $firstName = $user->firstname;
        }

        $user_initials =  $this->user_model->get_all_user_initials();
     
        
        $data['quote_result'] = $quote_result; 
        $data['user_info'] = $user_info;
        $data['firstname'] = $firstName;
        $data['request'] = $request;
        $data['nomatch'] = $nomatch; 
        $data['user_initials'] = $user_initials; 
        $data['requestId'] = $requestId;
 
        $this->load->view('includes/header', $data);
        $this->load->view('customer/dashboard', $data);
        $this->load->view('includes/footer');		
		 
    }

    function update_search($request_id){
        $data = array(  
            'zip_code' => $this->input->post('zipcode'),
            'miles' => $this->input->post('miles')
        ); 
        $customer_request_id = $this->register_model->update_customer_request_byId($data, $request_id);

        redirect('customer');
    }

    function remove_quote($qoute_id){
        $result = $this->vehicle_model->delete_quote($qoute_id);

        if($result)
            redirect('customer');
    }
    
    function schedule_appointment(){
        
        $user_id =  $this->session->userdata('user_id');         
        $level = $this->session->userdata('level');
        $postData = $this->input->post();   

        $vehicle_quote_data = array(
            'appointment_date' => date("Y-m-d", strtotime($postData['date'])), 
            'appointment_time' => date("H:i a", strtotime($postData['time'])), 
        ); 
        $this->vehicle_model->update_vehicle_quotes($vehicle_quote_data,$postData['vehicle-quote-id']);
            
        $this->send_email($postData);
		redirect('customer/index');
    } 

    function send_email($data)
    {         

        $message_header = "You have a new appointment!";   

        $email_to = $data['dealer-email'];           

		$email_data = array(
            'customer_fname' => $data['firstname'],
            'customer_lname' => $data['lastname'],
            'customer_email' => $data['email'],
            'customer_phone' => $data['phone'],
            'appointment_date' => $data['date'], 
            'appointment_time' => $data['time'], 
            'dealership_name' => $data['dealership-name'],
            'dealer_name' => $data['dealer-name'],
            'dealership_address' => $data['dealership-address'],
            'dealers_email' => $data['dealer-email'],
        );  
		
		$message = $this->load->view('customer/emailappointment',$email_data,TRUE);
		
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("jointhequote@jointhequote.com", "Join the Quote");
        $email->setSubject("New Appointment Scheduled");
        $email->setReplyTo('jointhequote@jointhequote.com');
        $email->addTo("$email_to", "Admin User");
        $email->addContent(
            "text/html", "$message"
        );

        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $response_code = $response->statusCode();             

        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
  
        return $response_code;           
        

    }
   
}