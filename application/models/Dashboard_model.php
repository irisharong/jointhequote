<?php

class Dashboard_model extends CI_Model {
        
    function count_inbox(){
        $id = $this->session->userdata('user_id');
        $q = "select * from tbl_message where user_to=$id";
        $rs = $this->db->query($q);
        return $rs->num_rows();
    }
    
    function count_sent(){
        $id = $this->session->userdata('user_id');
        $q = "select * from tbl_message_sent where user_from=$id";
        $rs = $this->db->query($q);
        return $rs->num_rows();
    }
    
    function count_messages(){
            $id = $this->session->userdata('user_id');
            $data['inbox'] = $this->db
                    ->where('user_to',$id)
                    ->count_all_results('tbl_message');
            $data['sent'] = $this->db
                    ->where('user_from',$id)
                    ->count_all_results('tbl_message_sent');            
            return $data;
        }
    
    function count_inserted($date){
            $id = $this->session->userdata('user_id');
            $data['inbox'] = $this->db
                   ->where('user_to',$id)
                    ->like('date',$date,'both')
                    ->count_all_results('tbl_message');
            
            $data['sent'] = $this->db
                    ->where('user_from',$id)
                    ->like('date',$date,'both')
                    ->count_all_results('tbl_message_sent');

            return $data;
        }
}   
?>