<!-- Join the Quote
================================================== -->
<div class="dashboard-content-container" id="join-the-quote" data-simplebar >
	<form method="post" id="join-the-quote-form" action="<?php echo base_url('dealer/submit_quote'); ?>">
		<div class="dashboard-content-inner margin-bottom-50" >			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Join The Quote</h3>
			</div>
			<div class="row">		
				<div class="col-xl-6 col-md-6">
					<table class="basic-table request">
						<tr>
							<th>Customer Vehicle Request Details</th>
						</tr>
						<tr>
							<table class="basic-table">
								<?php $trade=""; ?>        
								<?php foreach($customer_request_row as $cr_data): ?>  
								<?php 
									$trade = $cr_data->trade_in;
								?>
								<input type="hidden" name="cr-id" value="<?php echo  $cr_data->id; ?>" />
								<input type="hidden" name="cr-uid" value="<?php echo  $cr_data->user_id; ?>" />
								<input type="hidden" name="cr-make" value="<?php echo  $cr_data->make; ?>" />
								<input type="hidden" name="cr-model" value="<?php echo  $cr_data->model; ?>" />
								<tr>
									<td data-label="Column 1">Make</td>
									<td data-label="Column 2"><?php echo $cr_data->make?></td> 
								</tr>
								<tr>
									<td data-label="Column 1">Model</td>
									<td data-label="Column 2"><?php echo $cr_data->model?></td> 
								</tr>
								<tr>
									<td data-label="Column 1">Year Range</td>
									<td data-label="Column 2"><?php echo $cr_data->year_from." - ".$cr_data->year_to?></td> 
								</tr>
								<tr>
									<td data-label="Column 1">Mileage Range</td>
									<td data-label="Column 2"><?php $mileage_from = "";
																	$mileage_to = "";
																	if (!empty($cr_data->mileage_from) || $cr_data->mileage_from != null) {
																		$mileage_from = number_format($cr_data->mileage_from);
																	}
																	if (!empty($cr_data->mileage_to) || $cr_data->mileage_to != null){
																		$mileage_to = number_format($cr_data->mileage_to);
																	}																	 
                     
																	echo $mileage_from." - ".$mileage_to?></td>
								</tr>
								<tr>
									<td data-label="Column 1">Price Range</td>
									<td data-label="Column 2"><?php $price_from = "";
																	$price_to = "";
																	if (!empty($cr_data->price_from) || $cr_data->price_from != null) {
																		$price_from = number_format($cr_data->price_from);
																	}
																	if (!empty($cr_data->price_to) || $cr_data->price_to != null) {
																		$price_to = number_format($cr_data->price_to);
																	}
																	echo "$".$price_from." - $".$price_to?></td> 
								</tr>
								 
								<?php endforeach ?>					
							</table>
						</tr>
					</table>
				</div>
				<div class="col-xl-6 col-md-6">
					<table class="basic-table">
						<tr>
							<th>Your Vehicle Selection</th>
						</tr>
						<tr> 
							<table class="basic-table" id="vehicle-details" style="display:none"> 							
							</table> 
						</tr>
						<tr> 	
							<div id ="vehicle-selection" class="basic-table">
								<select  class="selectpicker" data-live-search="true" name="dealer-vehicles" id="dealer-vehicles" title="Type vehicle name">
									      
									<?php foreach($all_vehicle as $vehicle): ?>  
									 
										<option value="<?php echo $vehicle->id?>"><?php echo $vehicle->make." ".$vehicle->model." ".$vehicle->trim?> </option>
									<?php endforeach ?>
								</select> 
							</div>
						</tr>
					</table>
				</div>			
			</div>	
			<div class="col-xl-12 margin-top-70">
				<div class="col-xl-6 float-left margin-bottom-60">
					<?php if($trade) { ?>
					<table class="basic-table trade">
						<tr>
							<th>Customer Vehicle Trade-In Details</th>
						</tr>
						<tr> 
							<table class="basic-table" id="trade-in-details"> 	
							<?php $cnt=0 ?>        
							<?php foreach($trade_in_dtls as $trade_in): ?>  
							<?php $cnt++; ?>
							<tr>
								<td data-label="Column 1">Make</td>
								<td data-label="Column 2"><?php echo $trade_in->make?></td> 
							</tr>
							<tr>
								<td data-label="Column 1">Model</td>
								<td data-label="Column 2"><?php echo $trade_in->model?></td> 
							</tr>	
							<tr>
								<td data-label="Column 1">Trim</td>
								<td data-label="Column 2"><?php echo $trade_in->trim?></td> 
							</tr>
							<tr>
								<td data-label="Column 1">Year</td>
								<td data-label="Column 2"><?php echo $trade_in->year?></td> 
							</tr>
							<tr>
								<td data-label="Column 1">Price</td>
								<td data-label="Column 2"><?php $price = "";
																if (!empty($trade_in->price) || $trade_in->price != null) {
																	$price = number_format($trade_in->price);
																}
																echo "$".$price?></td> 
							</tr>
							<tr>
								<td data-label="Column 1">Internal Condition</td>
								<td data-label="Column 2"><?php echo $trade_in->internal?></td> 
							</tr>
							<tr>
								<td data-label="Column 1">External Condition</td>
								<td data-label="Column 2"><?php echo $trade_in->external?></td> 
							</tr>	
							<?php endforeach ?>						
							</table> 
						</tr>
					</table>
					<?php } ?>
				</div>
				 	
				<div class="col-xl-6 float-right" id="offer" style="display:none">	
				<?php if($trade) { ?>
					<div class="submit-field">	
						<label>What are you willing to offer for the customer trade in?</label>
						<input name="trade-in-offer" id="trade-in-offer" placeholder="Enter trade in value you're willing to offer">
					</div>		
				<?php } ?>
					<div class="submit-field">
						<label>What add-ons are you willing to offer to the customer?</label>
						<input type="text" name="note" placeholder="Ex: 3 Free Oil Changes" value="" />
					</div>
				</div> 
				<div class="col-xl-6 float-left" id="add-quote" style="display:none">
					<div class="submit-field">	
						<input type="submit" value="Add Quote" class="button ripple-effect big margin-top-30" /> 
						<input type="hidden" value="add" id="submit-button" name="submit-button" /> 
					</div>
				</div>				 
			</div>
		</div>		   
	</form>
</div>
  