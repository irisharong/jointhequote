<?php

class User_model extends CI_Model {
    
    function user_info(){
        $user_id = $this->session->userdata('user_id');
        $q = $this->db
                    ->where('id',$user_id)
                    ->get('user');
        return $q->result();
    }

    function get_user_by_email($email){
        $q = $this->db
                ->where('email',$email)
                ->get('user');
        return $q->result();
    }
     
    function get_contacts(){
        $id = $this->session->userdata('user_id');
        $q = $this->db
                ->where('id!=',$id)
                ->get('user')->result();
        return $q;
    }
    function get_all_users(){ 
        $q = $this->db
                    ->where('status=',1)  
                    ->get('user')                    
                    ->result();
        return $q;
    }

    function get_all_user_initials(){
        $q = 'SELECT id, firstname, lastname
        FROM user 
        WHERE status = 1
        ';
        $q = $this->db->query($q)->result();
        $initials = array();
        foreach($q as $user){
            $firstname = $user->firstname;
            $lastname = $user->lastname;
            $initials[$user->id] = $firstname[0]."".$lastname[0];
        }
        return $initials;
    }
    
    function get_users_by_search(){
        $keyword = $this->input->post('keyword');
        $id = $this->session->userdata('user_id');
        $q = 'SELECT * FROM user WHERE id!='.$id.'
            AND (
                id_no LIKE "%'.$keyword.'%"
                OR firstname LIKE "%'.$keyword.'%"
            )
            ORDER BY firstname ASC
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }
    
    
    function check_id($id){
        $user_id = $this->input->post('user_id');
        $q = $this->db->where('id_no',$id)
                      ->where('id!=',$user_id)
                      ->get('user');
        if($q->num_rows() > 0){
            return true;   
        }else{
            return false;   
        }
    }
    
    function check_email($username){
        $user_id = $this->input->post('user_id');
        $q = $this->db->where('email',$username)
                      ->where('id!=',$user_id)
                      ->get('user');
        if($q->num_rows() > 0){
            return true;   
        }else{
            return false;   
        }
    }

    function mail_exists($key)
    {
        $this->db->where('email',$key);
        $query = $this->db->get('user');
        if ($query->num_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    function update_user(){
        $user_id = $this->input->post('user_id');
        $data = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email')
            );
        $password = $this->input->post('password');
        if($password):
            $data['password'] = $password; 
        endif;
        $this->db->where('id',$user_id);
        $this->db->update('user',$data);        
    }
    
    function delete_user(){
        $ids = $this->input->post('user_id');
        $c = count($ids);
        for($i=0; $i < $c; $i++){
            $user_id = $ids[$i];   
            $this->db
                    ->where('id',$user_id)
                    ->delete('user'); 
        }
        
    }

    function get_dealer_id($user_id){
        $q = 'SELECT dealer_id
        FROM user 
        WHERE id = "'.$user_id.'"
        ';
        $q = $this->db->query($q)->result();
        foreach($q as $id){
            $dealer_id = $id->dealer_id;
        }
        return $dealer_id;
    }

    function get_dealer_info($dealer_id){
        $q = 'SELECT *
        FROM dealership 
        WHERE dealer_id = "'.$dealer_id.'"
        ';
        $q = $this->db->query($q)->result();
         
        $dealership = array();
       
        foreach($q as $dealer){ 
            $dealership['id'] = $dealer->dealer_id;
            $dealership['dealership_name'] = $dealer->dealership_name;
            $dealership['address'] = $dealer->address;
            $dealership['zipcode'] = $dealer->zipcode;
            $dealership['dealership_phone'] = $dealer->dealership_phone;
        }
        return $dealership;
    }

    function get_user_info($id)
    {
        $q = 'SELECT *
            FROM user 
            WHERE id = "'.$id.'"
            ';
        $q = $this->db->query($q)->result();
        return $q; 
    }
    
    function update_password($email, $new_password){ 
        $data = array( 
                'password' => md5($new_password)
            );
       
        $this->db->where('email',$email);
        $this->db->update('user',$data);  

        return true;
 
    }
 
}

?>