<div class="container col-xl-4 padding-top-50">
   
    <!-- Welcome Text -->
    <div class="welcome-text">
        <h3>Reset Password</h3>
        
    </div> 

    
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>
        
        <?php if(isset($sent)){ ?>
        <div class="col-sm-12 margin-top-20">
            <div class="notification success closeable" id="email-sent">
                <strong>Email Sent!</strong> Please check your email and click the secure link.
            </div>
        </div> 
        <?php } ?>
        <!-- Form -->
        <form method="post" id="login-form" action="<?php echo base_url('user/reset'); ?>">
            <div class="input-with-icon-left">
                <i class="icon-material-baseline-mail-outline"></i>
                <input type="text" class="input-text with-border" name="email" id="email" placeholder="Email Address" required/>
            </div>

            <!-- Button -->
            <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form">Reset <i class="icon-material-outline-arrow-right-alt"></i></button>
            
        </form>
    
    
</div>