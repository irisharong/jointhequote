<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

use Twilio\Rest\Client;

class Dealer extends CI_Controller {

	public function __construct(){
        parent::__construct();    
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->model('register_model');
        $this->load->model('message_model');
        $this->load->model('vehicle_model');
        $this->load->model('settings_model');
        $this->load->library("geozip");
        $this->login_model->is_logged_in();   
        $level = $this->session->userdata('level');
        if($level == 'user'){  
            redirect(base_url());
        } 
 
       
    }
    
    function team(){
        $user = $this->user_model;
        
        $data['record'] = $user->get_all_users();  
        if(!$data['record']){
            $data['nodata'] = TRUE;           
        }
 
        $data['level'] = $this->session->userdata('level');
        $data['menu'] = 'team';
        $data['user_id'] = $this->session->userdata('user_id'); 

        if ($this->form_validation->run() == FALSE)
		{            
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar', $data);
            $this->load->view('dealer/team', $data);
			$this->load->view('includes/footer');
		}
		else
		{
            $user->update_user();
            redirect('users?update');
		}
    }

    function addMember(){
        $user = $this->user_model;
        
        $data['record'] = $user->get_all_users();  
        if(!$data['record']){
            $data['nodata'] = TRUE;           
        }
        $user_id = $this->session->userdata('user_id');
        $data['title'] = 'Users';
        $data['menu'] = 'team';
        $data['user_id'] = $user_id;


        $this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');  
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5'); 

        if ($this->form_validation->run() == FALSE)
		{
			
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar', $data);
            $this->load->view('dealer/add_member', $data);
			$this->load->view('includes/footer');
		}
		else
		{  
            $this->register_model->add_member($user_id);
            redirect('dealer/team');
		}
    }
    
    function settings(){
        $user = $this->user_model;
        
        $user_id = $this->session->userdata('user_id'); 
        $data['userInfo'] = $this->user_model->user_info();
        $data['level'] = $this->session->userdata('level');
        $data['title'] = 'My Team';
        $data['menu'] = 'settings';
        $data['user_id'] = $user_id;

        $dealer_id = $user_id;   //if manager/dealer admin 
        //get dealer id if salesperson
        $data['dealer'] = true;
        $level = $this->session->userdata('level');
        if($level == 'salesrep'){  
            $dealer_id = $this->user_model->get_dealer_id($user_id);
            $data['dealer'] = false;
        }

        $data['dealership'] = $this->settings_model->get_dealer_information($dealer_id); 
 
        if ($this->form_validation->run() == FALSE)
		{            
            $this->load->view('includes/header');
            $this->load->view('includes/sidebar', $data);
            $this->load->view('dealer/settings', $data);
			$this->load->view('includes/footer');
		}
		else
		{
            $user->update_user();
            redirect('dealer/settings');
		}
    }

    function request(){
     
        $user = $this->user_model;
        $user_id = $this->session->userdata('user_id'); 
        
        
        $data['record'] = $user->get_all_users();  
        if(!$data['record']){
            $data['nodata'] = TRUE;           
        }
 
        $data['level'] = $this->session->userdata('level');
        $data['menu'] = 'request';
        $data['user_id'] = $user_id; 

        //get dealer id if salesperson
        $dealer_id = $user_id;
        $level = $this->session->userdata('level');
        if($level == 'salesrep'){  
            $dealer_id = $this->user_model->get_dealer_id($user_id);
        }
        
        $dealer_info = $this->user_model->get_dealer_info($dealer_id);
        
        //get all customer request 
        $customer_request = $this->vehicle_model->get_customer_request_all();
        $quote = array();
        $quote_num = array();
        $submitted_quote = array();
        $request = array(); 
        
        foreach($customer_request as $req){
            //get the distance for both zipcode           
            $distance = $this->geozip->get_distance($req->zip_code, $dealer_info['zipcode']);
            
            if($distance !== false){ 
               //check if distance match with request miles
               if($distance <= $req->miles){ 
                    $request[$req->id] = $req; 
                    $quote[$req->id] = $this->vehicle_model->get_quote_by_request($req->id, $user_id);
                    $quote_num[$req->id] = $this->vehicle_model->get_num_quotes($req->id);

                    $submitted_quote[$req->id] = $this->vehicle_model->get_quote_submitted($req->id,$user_id);
               }  
            } 
           
        }
        $user = $this->user_model->user_info();
        foreach($user as $info){
            $first_name = $info->firstname;
        }
         
        $user_initials =  $this->user_model->get_all_user_initials();
     
        //get all appointments
        $appointments = array();
        $appointments = $this->vehicle_model->get_all_appointments($user_id);

        $data['user_info'] = $first_name; 
        $data['firstname'] = $first_name;  
        $data['customer_request'] = $request; 
        $data['quote'] = $quote; 
        $data['quote_num'] = $quote_num; 
        $data['submitted_quote'] = $submitted_quote; 
        $data['appointments'] = $appointments; 
        $data['user_initials'] = $user_initials; 
            
    
        $this->load->view('includes/header', $data);
        $this->load->view('includes/sidebar', $data);
        $this->load->view('dealer/request', $data);
        $this->load->view('includes/footer');
		
    }
    
    function messages(){
        
        $user_id =  $this->session->userdata('user_id'); 
        $data['title'] = 'My Team';
        $data['menu'] = 'messages';
        $data['user_id'] = $user_id; 
        $data['level'] = $this->session->userdata('level');

        $user_info = array();
        $messages = array();
        $all_messages = $this->message_model->get_messages();
        $cnt = 0;
        foreach($all_messages as $message){ 

            if($message->user_from != $user_id){
                $user_info[$message->user_from]['user_name'] =  $this->message_model->get_firstname($message->user_from); 
                $user_info[$message->user_from]['user_id'] =   $message->user_from;
            }
 

            $cnt++;
        }
        $data['user_info'] = $user_info;
        $data['messages'] = $messages; 
//var_dump("<pre>", $messages); die();
        $this->load->view('includes/header');
        $this->load->view('includes/sidebar', $data);
        $this->load->view('dealer/messages', $data);
        $this->load->view('includes/footer');
    }
 
    function join_the_quote(){
        
        $user_id =  $this->session->userdata('user_id'); 
        $postData = $this->input->post(); 

        $data['title'] = 'My Team';
        $data['menu'] = 'jointhequote';
        $data['user_id'] = $user_id;          
        $data['cr_id'] = $postData['cr_id'];  
        $data['level'] = $this->session->userdata('level');
          
        //get customer request by id, according to the clicked button
        $customer_request_row = $this->vehicle_model->get_customer_request_row($data['cr_id']); 
        
        //get dealer id if salesperson
        if($data['level']  == 'salesrep'){  
            $dealer_id = $this->user_model->get_dealer_id($user_id);
        } 

        //get trade-in details if trade-in is 1
        $trade_in_dtls = array();
        foreach($customer_request_row as $cr_data){
            if ($cr_data->trade_in == 1){
                $trade_in_dtls = $this->register_model->get_trade_in_details($data['cr_id']); 
            }
        }
        //get all vehicles under dealer id
        $all_vehicles_by_dealer = $this->vehicle_model->get_vehicle_by_dealer_id($dealer_id);
         
        $data['all_vehicle'] = $all_vehicles_by_dealer;  
        $data['customer_request_row'] = $customer_request_row;   
        $data['trade_in_dtls'] = $trade_in_dtls;  
        
        //var_dump("<pre>", $trade_in_dtls); die(); 

        $this->load->view('includes/header');
        $this->load->view('includes/sidebar', $data);
        $this->load->view('dealer/jointhequote', $data);
        $this->load->view('includes/footer');
    }

    function submit_quote(){
        
        $user_id =  $this->session->userdata('user_id');         
        $level = $this->session->userdata('level');
        $postData = $this->input->post();  
         
        $trade_in = (isset($postData['trade-in-offer'])) ? $postData['trade-in-offer'] : "";   

        $vehicle_quote_data = array(
            'vehicle_id' => $postData['dealer-vehicles'],
            'customer_request_id' =>  $postData['cr-id'],
            'customer_user_id' => $postData['cr-uid'],
            'dealer_id' => $user_id,
            'date_submitted' => date('Y-m-d G:i:s'), 
            'status' => 1, 
            'trade_in_offer' => $trade_in, 
            'note' => $postData['note'], 
        ); 

        if ($postData['submit-button'] == 'edit'){
            $vehicle_quote_data = array(
                'trade_in_offer' => $trade_in, 
                'note' => $postData['note'], 
            );  
            $this->vehicle_model->update_vehicle_quotes($vehicle_quote_data,$postData['vehicle-quote-id']);
        }else{            
            $this->vehicle_model->add_vehicle_quotes($vehicle_quote_data);
        }
 
        //get customer info
        $user_info = $this->user_model->get_user_info($postData['cr-uid']); 

        foreach($user_info as $info){
            $postData['firstname'] = $info->firstname;
            $postData['lastname'] = $info->lastname;
            $postData['email'] = $info->email;
            $postData['phone'] = $info->phone;

        }
        
       // var_dump("<pre>", $postData); die();       
        $this->send_email($postData);
		redirect('dealer/request');
    }

    function edit_quote(){
        
        $user_id =  $this->session->userdata('user_id'); 
        $postData = $this->input->post(); 

        $data['title'] = 'My Team';
        $data['menu'] = 'editquote';
        $data['user_id'] = $user_id;          
        $data['cr_id'] = $postData['cr_id'];   
        $data['level'] = $this->session->userdata('level'); 
           
        //get customer request by id, according to the clicked button
        $customer_request_row = $this->vehicle_model->get_customer_request_row($data['cr_id']);  
          
        //get trade-in details if trade-in is 1
        $trade_in_dtls = array();
        foreach($customer_request_row as $cr_data){
            if ($cr_data->trade_in == 1){
                $trade_in_dtls = $this->register_model->get_trade_in_details($data['cr_id']); 
            }
        }

        //get vehicle quote under dealer id
        $vehicle_quotes = $this->vehicle_model->get_vehicle_quotes($data['cr_id'],$user_id);
        
        $vehicles = array();
        foreach($vehicle_quotes as $quotes){
            $vehicles = $this->vehicle_model->get_vehicle_by_id($quotes->vehicle_id);  
            $data['trade_in_offer'] = $quotes->trade_in_offer;
            $data['note'] = $quotes->note; 
            $data['vehicle_id'] = $quotes->vehicle_id; 
            $data['vehicle_quote_id'] = $quotes->id; 
        }        
         
        $data['vehicles'] = $vehicles;  
        $data['customer_request_row'] = $customer_request_row;   
        $data['trade_in_dtls'] = $trade_in_dtls;  
 

        $this->load->view('includes/header');
        $this->load->view('includes/sidebar', $data);
        $this->load->view('dealer/editquote', $data);
        $this->load->view('includes/footer');
 
    }

    function inventory(){
        
        $user = $this->user_model;
        $user_id = $this->session->userdata('user_id'); 
        
        
        $data['level'] = $this->session->userdata('level');
        $data['menu'] = 'inventory';
        $data['user_id'] = $user_id; 

        //get dealer id if salesperson
        $level = $this->session->userdata('level');
        if($level == 'salesrep'){  
            $dealer_id = $this->user_model->get_dealer_id($user_id);
        } 

        //get all vehicles under dealer id
        $all_vehicles_by_dealer = $this->vehicle_model->get_vehicle_by_dealer_id($dealer_id);
        $data['all_vehicle'] = $all_vehicles_by_dealer;  
        
		$this->load->view('includes/header');
        $this->load->view('includes/sidebar', $data);
        $this->load->view('dealer/inventory', $data);
        $this->load->view('includes/footer');
    }
    

    function logout(){
        $this->session->sess_destroy();
		redirect('login');
    }   
    
    function check_id($str){
        $check_id = $this->user_model->check_id($str);
        if($check_id){
            $this->form_validation->set_message('check_id', 'ID no. '.$str.' is not available');
            return false;   
        }else{
            return true;   
        }
    }
    
    function check_email($str){
        $check_email = $this->user_model->check_email($str);
        if($check_email){
            $this->form_validation->set_message('check_email', 'Email ('.$str.') already exist.');
            return false;   
        }else{
            return true;   
        }
    }
    
    function get_info(){
        $info = $this->user_model->get_user_info();
        echo json_encode($info); 
    }
    
    function search(){
        $user = $this->user_model;
        $data['record'] = $user->get_users_by_search();
        if(!$data['record']){
            $data['nosearch'] = TRUE;   
        }
        $data['title'] = 'Search User';
        $data['main'] = 'users';
		$this->load->view('include/template',$data);
    }
    
    function delete(){
        $user = $this->user_model;
        $user->delete_user();
        redirect('users?delete');
    }
    function get_email($id){
        $email = $this->message_model->get_email($id);
        return $email;
    }
    
    function update(){
        $user = $this->user_model;
        
        
        
        
        redirect('users?update');
    }

    function send_email($data)
    {         

        $message_header = "New Dealer Quote For Your ".$data['cr-make']." ".$data['cr-model'];  
        $email_to = $data['email'];
		$email_data = array(
			'first_name' 		=> $data['firstname'],
			'last_name' 		=> $data['lastname'], 
			'email' 			=> $email_to,
			'phone' 			=> $data['phone'],  
			'make' 		    	=> $data['cr-make'],  
			'model' 			=> $data['cr-model'],  
			'message_header' 	=> $message_header,
        );  
		
		$message = $this->load->view('dealer/emailquote',$email_data,TRUE);
		
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("jointhequote@jointhequote.com", "Join the Quote");
        $email->setSubject("New Dealer Quote For Your ".$data['cr-make']." ".$data['cr-model']);
        $email->setReplyTo('jointhequote@jointhequote.com');
        $email->addTo("$email_to", "Admin User");
        $email->addContent(
            "text/html", "$message"
        );

        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $response_code = $response->statusCode();             

        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }

        //var_dump("<pre>", $response); die();  
        return $response_code;           
        

    }
}