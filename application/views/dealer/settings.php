<div class="col-xl-8 margin-bottom-30"> 
 
  <div class="col-xl-4">
    <div class="with-padding padding-top-30 margin-bottom-30">
      <h3>Contact Details</h3>              
    </div> 

    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

    <!-- Form -->
    <form method="post" id="settings-form" action="<?php echo base_url('user/register'); ?>">
    <div class="content with-padding padding-bottom-10">
      <div class="row">
        <?php foreach($userInfo as $user): ?>
        <div class="col-xl-6">
          <div class="submit-field">
            <input type="text" class="input-text with-border" name="firstname" id="firstname" value="<?php echo $user->firstname?>" required/>
          </div>
        </div>

        <div class="col-xl-6">
          <div class="submit-field">
            <input type="text" class="input-text with-border" name="lastname" id="lastname" value="<?php echo $user->lastname?>" required/>
          </div>
        </div>
        
        <div class="col-xl-12">
          <div class="submit-field">
            <input type="text" class="input-text with-border" name="email" id="email" value="<?php echo $user->email?>" required/>
          </div>
        </div>

        <div class="col-xl-12">
          <div class="submit-field">
            <input type="text" class="input-text with-border" name="phone" id="phone" value="<?php echo $user->phone?>" required/>
          </div>
        </div>

        <div class="col-xl-12">
          <div class="submit-field">
            <input type="password" class="input-text with-border" name="password" id="password" value="<?php echo $user->password?>" required/>
          </div>
        </div>

        <?php endforeach ?>
      </div>
    </div>
    </form> 
  </div>


  <div class="with-padding padding-top-30 margin-bottom-30">
    <h3>Dealership Details</h3>              
  </div> 

  <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

  <!-- Form --> 
  <div class="content with-padding padding-bottom-10">
    <div class="row">
      <?php $disabled = (!$dealer) ? "disabled" : "" ?>
      <?php foreach($dealership as $dealer):?>
      <div class="col-xl-4">
        <div class="submit-field">
          <h5>Dealer Name</h5>
          <input type="text" class="input-text with-border"  name="dealershipname" id="dealername" value="<?php echo $dealer->dealership_name;  ?>" required <?php echo $disabled; ?> />
        </div>
      </div>

      <div class="col-xl-4">
        <div class="submit-field">
          <h5>Dealership ID</h5>
          <input type="text" class="input-text with-border" name="dealershipid" id="dealershipid" value="<?php echo $dealer->dealer_id  ?>" required <?php echo $disabled; ?>/>
        </div>
      </div>
      
      <div class="col-xl-4">
        <div class="submit-field">
          <h5>Logo</h5>
          <input type="text" class="input-text with-border" name="logo" id="logo" value="<?php echo $dealer->logo  ?>" required <?php echo $disabled; ?>/>
        </div>
      </div>

      <div class="col-xl-4">
        <div class="submit-field">
          <h5>Address</h5>
          <input type="text" class="input-text with-border" name="address" id="address" value="<?php echo $dealer->address  ?>" required <?php echo $disabled; ?>/>
        </div>
      </div>

      <div class="col-xl-4">
        <div class="submit-field">
          <h5>Zipcode</h5>
          <input type="text" class="input-text with-border" name="address" id="address" value="<?php echo $dealer->zipcode  ?>" required <?php echo $disabled; ?>/>
        </div>
      </div>

      <div class="col-xl-4">
        <div class="submit-field">
          <h5>Dealership Phone</h5>
          <input type="text" class="input-text with-border" name="phone" id="paphonessword" value="<?php echo $dealer->dealership_phone  ?>" required <?php echo $disabled; ?>/>
        </div>
      </div>
      <?php endforeach ?>
    </div>
  </div> 

</div>

<div class="col-xl-6 margin-bottom-30"> 
 
</div>
    
   
