
	<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<div class="container">
				<div class="row">
				
					<div class="col-xl-12 col-md-12">

						<div class="section-headline margin-bottom-30">
							<h4>My Team</h4>
						</div> 
              
            <a href="#sign-in-dialog" class="popup-with-zoom-anim button gray ripple-effect pull-right margin-bottom-30"><i class="icon-material-outline-add"></i>New Team Member</a>
             
						<table class="basic-table">

							<tr>
								<th>Name</th>  
								<th>Email</th>
								<th>Role</th> 
								<th>Action</th> 
							</tr>
 					 
              <?php foreach($record as $row): ?>
							<tr>
								<td><?php echo $row->firstname." ".$row->lastname; ?></td>  
								<td><?php echo $row->email; ?></td>
								<td><?php echo $row->level; ?></td> 
								<td> </td>
								
							</tr>                
              <?php endforeach; ?>

						</table>
					</div>

				</div>

			</div>

		</div>
	</div>
	<!-- Dashboard Content / End -->
  

  <!-- Sign In Popup
================================================== -->
<div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide dialog-with-tabs">

<!--Tabs -->
<div class="sign-in-form">

  <ul class="popup-tabs-nav">
    <li><a href="#login">Add New Member</a></li> 
  </ul>

  <div class="popup-tabs-container">

    <!-- Login -->
    <div class="popup-tab-content" id="login">
       
      <!-- Form -->
      <!-- Form -->
      <form method="post" id="add-form" action="<?php echo base_url('dealer/addMember'); ?>">
        <div class="content with-padding padding-bottom-10">
          <div class="row">

            <div class="col-xl-6">
              <div class="submit-field">
                <input type="text" class="input-text with-border" name="firstname" id="firstname" placeholder="First Name" required/>
              </div>
            </div>

            <div class="col-xl-6">
              <div class="submit-field">
                <input type="text" class="input-text with-border" name="lastname" id="lastname" placeholder="Last Name" required/>
              </div>
            </div>
            
            <div class="col-xl-12">
              <div class="submit-field">
                <input type="text" class="input-text with-border" name="email" id="email" placeholder="Email" required/>
              </div>
            </div>

            <div class="col-xl-12">
              <div class="submit-field">
                <input type="text" class="input-text with-border" name="phone" id="phone" placeholder="Phone" required/>
              </div>
            </div>

            <div class="col-xl-12">
              <div class="submit-field">
                <input type="password" class="input-text with-border" name="password" id="password" placeholder="Password" required/>
              </div>
            </div>

            <!-- Button -->
            <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="add-form">Add Member <i class="icon-material-outline-arrow-right-alt"></i></button>
              
          </div>
        </div>
      </form>
       

    </div>
 

  </div>
</div>
</div>
<!-- Sign In Popup / End -->