<!-- Inventory
================================================== -->
<div class="dashboard-content-container" id="inventory" data-simplebar >
		<div class="dashboard-content-inner margin-bottom-100" >			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Inventory</h3>
			</div>
			<div class="row">		
				<div class="col-xl-12 col-md-12">
					<table class="basic-table" >
						<tr>
							<th>Vehicle Details</th>
						</tr>
						<?php $cnt=0 ?>        
            <?php foreach($all_vehicle as $vehicle): ?>  
            <?php $cnt++; ?>
            <tr>
              <td><strong><?php echo $vehicle->make." ".$vehicle->model." ".$vehicle->trim?></strong><br> 
                  <span>Year: <?php echo $vehicle->year?><span><br>
									<span>Mileage: <?php  $mileage = "";
																				if (!empty($vehicle->mileage) || $vehicle->mileage != null){ 
																					$mileage = number_format($vehicle->mileage);
																				} 
																				echo $mileage?><span>
              </td>
            </tr>
            <?php endforeach ?>
					</table>
				</div>							
			</div>		
		</div>		
</div>
  