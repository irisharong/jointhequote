<!doctype html>
<html lang="en">
<head>

<!-- Basic Page Needs
================================================== -->
<title>Join The Quote</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="keywords" content="" />
<meta name="description" content="Join The Quote" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS
================================================== -->
<link rel="stylesheet" href="<?php echo base_url('assets/css/style.css'); ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/css/colors/blue.css'); ?>">


</head>
<body>

<!-- Wrapper -->
<div id="wrapper">


<!-- Header Container
================================================== -->
<header id="header-container" class="fullwidth ">

	<!-- Header -->
	<div id="header">
		<div class="container">
			
			<!-- Left Side Content -->
			<div class="left-side">
				
				<!-- Logo -->
				<div id="logo">
					<a href="#"><img src="<?php echo base_url('assets/images/logo.png')?>" alt=""></a>
				</div>

				 
				
			</div>
			<!-- Left Side Content / End -->

			<?php if(isset($user_id)){ ?>
			<!-- Right Side Content / End -->
			<div class="right-side">

			<!-- User Menu -->
			<div class="header-widget">
				<!-- Messages -->
				<div class="header-notifications user-menu">
					<div class="header-notifications-trigger">
						<a href="#"><div><strong>Hello, <?php echo $firstname?>!</strong> </div></a>
					</div>

					<!-- Dropdown -->
					<div class="header-notifications-dropdown">							
						<ul class="user-menu-small-nav"> 
							<li><a href="#"><i class="icon-material-outline-settings"></i> Settings</a></li>
							<li><a href="<?php echo base_url('user/logout')?>"><i class="icon-material-outline-power-settings-new"></i> Logout</a></li>
						</ul>

					</div>
				</div>

				</div>
				<!-- User Menu / End -->
			 
				<!-- Mobile Navigation Button -->
				<span class="mmenu-trigger">
					<button class="hamburger hamburger--collapse" type="button">
						<span class="hamburger-box">
							<span class="hamburger-inner"></span>
						</span>
					</button>
				</span>

			</div>
			<!-- Right Side Content / End -->

			<?php } ?>

			
			 

		</div>
	</div>
	<!-- Header / End -->

</header>
<div class="clearfix"></div>
<!-- Header Container / End -->


