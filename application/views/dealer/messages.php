<!-- Dashboard Content
	================================================== -->
	<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			<!-- Dashboard Headline -->
			<div class="dashboard-headline">
				<h3>Messages</h3>
 
				<div class="messages-container margin-top-0">

					<div class="messages-container-inner">

						<!-- Messages -->
						<div class="messages-inbox">
							<div class="messages-headline">
								<div class="input-with-icon">
										<input id="autocomplete-input" type="text" placeholder="Search">
									<i class="icon-material-outline-search"></i>
								</div>
							</div>

							<ul>
              <?php foreach($user_info as $user ){ ?>
							 
								<li>
									<a href="#read" class="read_message" data-id="<?php echo $user['user_id']; ?>">
										<div class="message-avatar"><i class="status-icon status-offline"></i><img src="images/user-avatar-small-02.jpg" alt="" /></div>

										<div class="message-by">
											<div class="message-by-headline">
												<h5><?php echo $user["user_name"]  ?></h5>
												<span><?php //echo $msg->date; ?></span>
											</div>
											<p></p>
										</div>
									</a>
								</li>
								 
                <?php } ?>
							</ul>
						</div>
						<!-- Messages / End -->

						<!-- Message Content -->
						<div class="message-content">

              
							<div class="messages-headline">
								<h4> </h4>
								<a href="#" class="message-action"><i class="icon-feather-trash-2"></i> Delete Conversation</a>
							</div>
							
             
							<!-- Message Content Inner -->
							<div id="message-content" class="message-content-inner">
              <?php foreach($messages as $user): ?>
									<!-- Time Sign -->
									<div class="message-time-sign">
										<span>28 June, 2018</span>
									</div>
                  <?php foreach($user as $msg): ?>
                  <!-- Time Sign --> 
									<div class="message-bubble">
										<div class="message-bubble-inner">
											<div class="message-avatar"><img src="images/user-avatar-small-01.jpg" alt="" /></div>
											<div class="message-text"><p><?php echo $msg['content']?></p></div>
										</div>
										<div class="clearfix"></div>
									</div>
                  <?php endforeach ?>
							<?php endforeach ?>
							</div>
							<!-- Message Content Inner / End -->
							
							<!-- Reply Area -->
							<div class="message-reply">
								<textarea id="textbox" cols="1" rows="1" placeholder="Your Message" data-autoresize></textarea>
								<button id="send" class="button ripple-effect">Send</button>
							</div>
              
						</div>
						<!-- Message Content -->

					</div>
			</div>
			<!-- Messages Container / End -->

 

		</div>
	</div>
	<!-- Dashboard Content / End -->