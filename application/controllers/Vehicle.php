<?php 
 
class Vehicle extends CI_Controller{
    function __construct()
    {
        parent::__construct(); 
        $this->load->library('form_validation'); 
        
        $this->load->library('session'); 
        $this->load->model('vehicle_model');
        
        
        $this->load->library('form_validation'); 
    } 

    
    function get_vehicle_model(){
        
        $make = $this->input->post('make');          
        $model =  $this->vehicle_model->get_vehicle_model($make);
         
        echo json_encode($model);
         
    }
 
    function get_vehicle_trim(){
        $model = $this->input->post('model');  
        $trim =  $this->vehicle_model->get_vehicle_trim($model);
        
        echo json_encode($trim);
         
    }

    function get_vehicle_by_id(){
        $vehicle_id = $this->input->post('id');  
        $vehicle_dtls =  $this->vehicle_model->get_vehicle_by_id($vehicle_id);
        
        echo json_encode($vehicle_dtls);
         
    }

 
}
