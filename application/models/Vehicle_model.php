<?php

class Vehicle_model extends CI_Model {
    
    function get_all_make(){
        $q = 'SELECT DISTINCT(make)
            FROM vehicle_data 
            WHERE 1 
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }

    function get_vehicle_model($make){ 
        $q = 'SELECT DISTINCT(model)
        FROM vehicle_data 
        WHERE make LIKE "%'.$make.'%"
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }

    function get_vehicle_trim($model){
        $q = 'SELECT DISTINCT(trim)
        FROM vehicle_data 
        WHERE model LIKE "%'.$model.'%"
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }

    function get_all_year(){
        $q = 'SELECT DISTINCT(year)
            FROM vehicle_data 
            ORDER BY year DESC 
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }
 
    function get_customer_request($user_id){ 
        $q = 'SELECT cr.*, u.id as user_id, u.firstname as firstname, u.lastname as lastname, u.email as email,u.phone as phone
        FROM customer_request cr
        LEFT JOIN user u ON u.id = cr.user_id
        WHERE user_id = '.$user_id;
 
        $q = $this->db->query($q)->result();
        
        $request = array();
        foreach($q as $req){
            $request[] = $req;
        }
       
        return $request;
    }

    function get_request_by_zipcode($zipcodes){
        $zips = implode(', ', $zipcodes);// var_dump($zips);die();
        $q = 'SELECT cr.*, u.id as user_id, u.firstname as firstname, u.lastname as lastname, u.email as email 
        FROM customer_request cr
        LEFT JOIN user u ON u.id = cr.user_id
        WHERE cr.zip_code IN ("'.$zips.'") AND cr.status= 1 ORDER BY id DESC';
 
        $q = $this->db->query($q)->result();
         
        return $q;
    }

    function get_customer_request_all(){
        $q = 'SELECT cr.*, u.id as user_id, u.firstname as firstname, u.lastname as lastname, u.email as email 
        FROM customer_request cr
        LEFT JOIN user u ON u.id = cr.user_id
        WHERE cr.status= 1 ORDER BY id DESC';
 
        $q = $this->db->query($q)->result();
         
        return $q;
    }

    function get_customer_request_row($cr_id){ 
        $q = 'SELECT cr.*
        FROM customer_request cr
        WHERE id = '.$cr_id;
 
        $q = $this->db->query($q)->result();
        
        $request = array();
        foreach($q as $req){
            $request[] = $req;
        }
       
        return $request;
    }

    function get_quotes($customer_request_id){
        $q = 'SELECT v.*, vq.id as quote_id, u.id as dealer_id, u.firstname as firstname, u.lastname as lastname, u.email as email,
                     d.dealership_name as dealership_name, d.address as dealer_address
            FROM vehicle_quotes vq
            LEFT JOIN vehicles v ON v.id = vq.vehicle_id
            LEFT JOIN user u ON u.id = vq.dealer_id
            LEFT JOIN dealership d ON d.dealer_id = v.dealer_id
            WHERE customer_request_id = "'.$customer_request_id.'" 
        '; 
        $q = $this->db->query($q)->result();
        return $q;

    }

    function get_quote_by_request($request_id, $user_id){
        $q = 'SELECT count(id)as count
            FROM vehicle_quotes 
            WHERE customer_request_id = "'.$request_id.'"
                 AND dealer_id = "'.$user_id.'"
        ';
        $q = $this->db->query($q)->result();
        foreach($q as $count){
            $quote = $count->count;
        }
        
        return $quote;
    }

    function get_num_quotes($request_id){
        $q = 'SELECT count(id)as count
            FROM vehicle_quotes 
            WHERE customer_request_id = "'.$request_id.'" 
        ';
        $q = $this->db->query($q)->result();
        foreach($q as $count){
            $quote = $count->count;
        }
        
        return $quote;
    }

    function get_quote_submitted($request_id, $user_id){
        $q = 'SELECT date_submitted
            FROM vehicle_quotes 
            WHERE customer_request_id = "'.$request_id.'"
                AND dealer_id = "'.$user_id.'"
        ';
        $q = $this->db->query($q)->result();

        $date_submitted = "";
        foreach($q as $date){
            $date_submitted = $date->date_submitted;
        }
        
        return $date_submitted;
    }

    function get_all_dealership(){
        $q = 'SELECT * 
        FROM dealership
        WHERE status = 1' ;
        $q = $this->db->query($q)->result();
        $dealer_zipcode = array();
        foreach($q as $dealer){
            $dealer_zipcode[] = $dealer->zipcode;
        } 
        return $dealer_zipcode; 
    }
 
    //check if there is customer request matches on dealers vehicle
    function get_quote_request_for_dealers($request, $user_id){ 

        $q = 'SELECT v.id as v_id, vq.id as quote_id
        FROM vehicles v 
        LEFT JOIN vehicle_quotes vq ON v.id = vq.vehicle_id 
        WHERE v.dealer_id = '.$user_id.' AND 
            (v.make LIKE "%'.$request->make.'%"
            OR v.model LIKE "%'.$request->model.'%"
            OR v.trim LIKE "%'.$request->trim.'%")         
        ';
        $matches = $this->db->query($q)->result();
        
        return $matches;
    }

    //check if there is customer request matches on customer request vehicle
    function get_match_request_for_customer($request){ 

        $q = 'SELECT v.id as v_id 
        FROM vehicles v  
        WHERE v.make LIKE "%'.$request->make.'%"
            AND v.model LIKE "%'.$request->model.'%"
            AND v.trim LIKE "%'.$request->trim.'%"         
        ';
        $matches = $this->db->query($q)->result();
        
        return $matches;
    }

    function update_vehicle_requests($search_result, $request_id){
        foreach($search_result as $request){ 
            $data = array(  
				'vehicle_id' => $request->id,
				'dealer_id' => $request->dealer_id,
				'customer_request_id' =>  $request_id,
				'status' =>  1
            );
                
            $q = $this->db->where($data)
                      ->get('vehicle_requests');
            if($q->num_rows() > 0){
               
                return 0;
            }else{ 
                $this->db->insert('vehicle_requests', $data); 
            }
        
        }

    }

    function get_vehicle_requests($id){
        $q = 'SELECT * 
            FROM vehicle_requests
            WHERE dealer_id = '.$id ;
        $q = $this->db->query($q)->result();
        return $q;
    }

    function get_vehicle_by_dealer_id($dealer_id){
        $q = 'SELECT * 
            FROM vehicles
            WHERE dealer_id = '.$dealer_id ;
        $q = $this->db->query($q)->result();
        return $q;
    }

    function get_vehicle_by_id($id){  
        $q = 'SELECT * 
            FROM vehicles
            WHERE id = '.$id ;
        $q = $this->db->query($q)->result();
        return $q;
    }
  
    function add_vehicle_quotes($data){
                
        $this->db->insert('vehicle_quotes',$data);    
        
    }

    function get_vehicle_quotes($cr_id,$dealer_id){
                
        $q = 'SELECT *
            FROM vehicle_quotes 
            WHERE customer_request_id = "'.$cr_id.'"
            AND dealer_id = "'.$dealer_id.'"
            ';
        $q = $this->db->query($q)->result();
        return $q;   
        
    }

    function get_all_appointments($dealer_id){
                
        $q = 'SELECT vq.*,v.make as v_make, v.model as v_model, v.trim as v_trim, v.year as v_year, v.mileage as v_mile,v.price as v_price,
                     cr.*,u.* 
            FROM vehicle_quotes vq 
            LEFT JOIN customer_request cr on cr.id = vq.customer_request_id 
            LEFT join vehicles v on v.id=vq.vehicle_id 
            LEFT JOIN user u ON u.id = vq.customer_user_id 
            WHERE vq.appointment_date IS NOT NULL AND vq.appointment_time IS NOT NULL AND vq.dealer_id = "'.$dealer_id.'"
            ';
        $q = $this->db->query($q)->result();
        return $q;    
    }
 
    function update_vehicle_quotes($data, $vq_id){
         
        $this->db->where('id', $vq_id);
        $this->db->update('vehicle_quotes', $data); 
        return true;
    }

    function delete_quote($qoute_id){
        $q = 'DELETE FROM `vehicle_quotes` WHERE id = '.$qoute_id;
        $q = $this->db->query($q);
        return true;   
 
    }
}
?>