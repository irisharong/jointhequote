<div class="container col-xl-4 padding-top-50">
   
    <!-- Welcome Text -->
    <div class="welcome-text">
        <h3>Reset Password</h3>
        
    </div> 

    
        <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>
        <div class="col-sm-12">
                <div class="alert alert-danger closeable" id="wrong-error" style="<?php if(!isset($error)){ ?>display: none;<?php } ?>">
                <strong>Error!</strong> Passwords do not match.
            </div>
        </div>  
        <div class="col-sm-12">
            <div class="notification warning closeable" id="save-error" style="<?php if(!isset($error2)){ ?>display: none;<?php } ?>">
                Something went wrong. Please try again!
            </div>
        </div>   
        <div class="col-sm-12">
            <div class="notification success closeable" id="save-error" style="<?php if(!isset($success)){ ?>display: none;<?php } ?>">
                Password has been updated! You may continue to <a href="<?php echo base_url('/login'); ?>">login</a> page. 
            </div>
        </div>   
 
        <!-- Form -->
        <form method="post" id="login-form" action="<?php echo base_url('user/reset_pass/').$email_encode; ?>">
            <div class="input-with-icon-left">
                <i class="icon-material-baseline-mail-outline"></i>
                <input type="password" class="input-text with-border" name="new_password" id="new_password" placeholder="New Password" required/>
            </div>
            <div class="input-with-icon-left">
                <i class="icon-material-baseline-mail-outline"></i>
                <input type="password" class="input-text with-border" name="re_password" id="re_password" placeholder="Re-type Password" required/>
            </div> 
            <!-- Button -->
            <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form">Update <i class="icon-material-outline-arrow-right-alt"></i></button>
            
        </form>
    
    
</div>