<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="container col-xl-6 margin-bottom-30">
					<div class="dashboard-box margin-top-0">
 
            <div class="welcome-text with-padding padding-top-30">
              <h3>Vehicle Request Form</h3>              
            </div> 

            <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

            <!-- Form -->
            <form method="post" id="vehicle-form" action="<?php echo base_url("user/request/$user_id"); ?>">
            <div class="content with-padding padding-bottom-10">
              <div class="row">
                
				<div class="col-xl-12">
					<div class="submit-field">
						<h5>Select Make</h5>
						<select name="vehicle_make" id="vehicle_make" class="selectpicker with-border" data-size="7" title="Make">
						<?php foreach($make as $key => $val):?>
							<option value="<?php echo $val->make?>"> <?php echo $val->make?></option> 
						<?php endforeach?>
						</select>
					</div>
				</div>

				<div class="col-xl-12">
					<div class="submit-field">
						<h5>Select Model</h5>
						<select name="vehicle_model" id="vehicle_model" class="selectpicker with-border" data-size="7" title="Model">
							<option>Please Select Make</option>
						</select>
					</div>
				</div>

				<div class="col-xl-12">
					<div class="submit-field">
						<h5>Select Trim</h5>
						<select name="vehicle_trim" id="vehicle_trim" class="selectpicker with-border" data-size="7" title="Trim">
							<option>Please Select Model</option>
							
						</select>
					</div>
				</div>

				<div class="col-xl-6">
					<div class="submit-field">
						<h5>Select Year</h5>
						<select name="year_from" class="selectpicker with-border" data-size="7" title="From">
							<?php foreach($year as $key): ?>
								<option value="<?php echo $key->year?>"><?php echo $key->year ?></option> 
							<?php endforeach?>
						</select>
					</div>
				</div>

				<div class="col-xl-6">
					<div class="submit-field">
						<h5>&nbsp;</h5>
						<select name="year_to" class="selectpicker with-border" data-size="7" title="To">
							<?php foreach($year as $key): ?>
								<option value="<?php echo $key->year?>"><?php echo $key->year ?></option> 
							<?php endforeach?>
						</select>
					</div>
				</div>

				<div class="col-xl-6">
					<div class="submit-field">
						<h5>Price Range</h5>
						<select name="price_from" class="selectpicker with-border" data-size="7" title="From">
							<option value="1">$1</option>
							<option value="1000">$1,000</option>
							<option value="2000">$2,000</option>
							<option value="3000">$3,000</option>
							<option value="4000">$4,000</option>
							<option value="5000">$5,000</option>
							<option value="6000">$6,000</option>
							<option value="7000">$7,000</option>
							<option value="8000">$8,000</option>
							<option value="9000">$9,000</option>
							<option value="10000">$10,000</option>
							<option value="11000">$11,000</option>
							<option value="12000">$12,000</option>
							<option value="13000">$13,000</option>
							<option value="14000">$14,000</option>
							<option value="15000">$15,000</option>
							<option value="16000">$16,000</option>
							<option value="17000">$17,000</option>
							<option value="18000">$18,000</option>
							<option value="19000">$19,000</option>
							<option value="20000">$20,000</option>
							<option value="21000">$21,000</option>
							<option value="22000">$22,000</option>
							<option value="23000">$23,000</option>
							<option value="24000">$24,000</option>
							<option value="25000">$25,000</option>
							<option value="26000">$26,000</option>
							<option value="27000">$27,000</option>
							<option value="28000">$28,000</option>
							<option value="29000">$29,000</option>
							<option value="30000">$30,000</option>
							<option value="31000">$31,000</option>
							<option value="32000">$32,000</option>
							<option value="33000">$33,000</option>
							<option value="34000">$34,000</option>
							<option value="35000">$35,000</option>
							<option value="36000">$36,000</option>
							<option value="37000">$37,000</option>
							<option value="38000">$38,000</option>
							<option value="39000">$39,000</option>
							<option value="40000">$40,000</option>
							<option value="41000">$41,000</option>
							<option value="42000">$42,000</option>
							<option value="43000">$43,000</option>
							<option value="44000">$44,000</option>
							<option value="45000">$45,000</option>
							<option value="46000">$46,000</option>
							<option value="47000">$47,000</option>
							<option value="48000">$48,000</option>
							<option value="49000">$49,000</option>
							<option value="50000">$50,000</option>
							<option value="51000">$51,000</option>
							<option value="52000">$52,000</option>
							<option value="53000">$53,000</option>
							<option value="54000">$54,000</option>
							<option value="55000">$55,000</option>
							<option value="56000">$56,000</option>
							<option value="57000">$57,000</option>
							<option value="58000">$58,000</option>
							<option value="59000">$59,000</option>
							<option value="60000">$60,000</option>
							<option value="61000">$61,000</option>
							<option value="62000">$62,000</option>
							<option value="63000">$63,000</option>
							<option value="64000">$64,000</option>
							<option value="65000">$65,000</option>
							<option value="66000">$66,000</option>
							<option value="67000">$67,000</option>
							<option value="68000">$68,000</option>
							<option value="69000">$69,000</option>
							<option value="70000">$70,000</option>
							<option value="71000">$71,000</option>
							<option value="73000">$73,000</option>
							<option value="74000">$74,000</option>
							<option value="75000">$75,000</option>
							<option value="76000">$76,000</option>
							<option value="78000">$78,000</option>
							<option value="79000">$79,000</option>
							<option value="80000">$80,000</option>
							<option value="81000">$81,000</option>
							<option value="82000">$82,000</option>
							<option value="83000">$83,000</option>
							<option value="84000">$84,000</option>
							<option value="85000">$85,000</option>
							<option value="87000">$87,000</option>
							<option value="88000">$88,000</option>
							<option value="89000">$89,000</option>
							<option value="90000">$90,000</option>
							<option value="91000">$91,000</option>
							<option value="93000">$93,000</option>
							<option value="94000">$94,000</option>
							<option value="95000">$95,000</option>
							<option value="96000">$96,000</option>
							<option value="97000">$97,000</option>
							<option value="98000">$98,000</option>
							<option value="99000">$99,000</option>
							<option value="100000">$100,000</option>
							<option value="102000">$102,000</option>
							<option value="104000">$104,000</option>
							<option value="106000">$106,000</option>
							<option value="107000">$107,000</option>
							<option value="109000">$109,000</option>
							<option value="110000">$110,000</option>
							<option value="113000">$113,000</option>
							<option value="114000">$114,000</option>
							<option value="115000">$115,000</option>
							<option value="116000">$116,000</option>
							<option value="119000">$119,000</option>
							<option value="120000">$120,000</option>
							<option value="121000">$121,000</option>
							<option value="124000">$124,000</option>
							<option value="125000">$125,000</option>
							<option value="127000">$127,000</option>
							<option value="129000">$129,000</option>
							<option value="130000">$130,000</option>
							<option value="134000">$134,000</option>
							<option value="135000">$135,000</option>
							<option value="148000">$148,000</option>
							<option value="153000">$153,000</option>
							<option value="155000">$155,000</option>
							<option value="157000">$157,000</option>
							<option value="159000">$159,000</option>
							<option value="161000">$161,000</option>
							<option value="173000">$173,000</option>
							<option value="174000">$174,000</option>
							<option value="175000">$175,000</option>
							<option value="184000">$184,000</option>
							<option value="185000">$185,000</option>
							<option value="189000">$189,000</option>
							<option value="199000">$199,000</option>
							<option value="223000">$223,000</option>
							<option value="228000">$228,000</option>
							<option value="240000">$240,000</option>
							<option value="245000">$245,000</option>
							<option value="249000">$249,000</option>
							<option value="250000">$250,000</option>
							<option value="270000">$270,000</option>
							<option value="280000">$280,000</option>
							<option value="290000">$290,000</option>
							<option value="300000">$300,000</option>
							<option value="320000">$320,000</option>
							<option value="340000">$340,000</option>
							<option value="349000">$349,000</option>
							<option value="350000">$350,000</option>
							<option value="390000">$390,000</option>
							<option value="400000">$400,000</option>
							<option value="401000">$401,000</option>
							<option value="410000">$410,000</option>
							<option value="420000">$420,000</option>
							<option value="437000">$437,000</option>
							<option value="493000">$493,000</option>
							<option value="600000">$600,000</option>
							<option value="750000">$750,000</option>
							<option value="800000">$800,000</option>
							<option value="889000">$889,000</option>
							<option value="999000">$999,000</option>
							<option value="1000000">$1,000,000</option>
						</select>
					</div>
				</div>

				<div class="col-xl-6">
					<div class="submit-field">
						<h5>&nbsp;</h5>
						<select name="price_to" class="selectpicker with-border" data-size="7" title="To">
							<option value="1">$1</option>
							<option value="1000">$1,000</option>
							<option value="2000">$2,000</option>
							<option value="3000">$3,000</option>
							<option value="4000">$4,000</option>
							<option value="5000">$5,000</option>
							<option value="6000">$6,000</option>
							<option value="7000">$7,000</option>
							<option value="8000">$8,000</option>
							<option value="9000">$9,000</option>
							<option value="10000">$10,000</option>
							<option value="11000">$11,000</option>
							<option value="12000">$12,000</option>
							<option value="13000">$13,000</option>
							<option value="14000">$14,000</option>
							<option value="15000">$15,000</option>
							<option value="16000">$16,000</option>
							<option value="17000">$17,000</option>
							<option value="18000">$18,000</option>
							<option value="19000">$19,000</option>
							<option value="20000">$20,000</option>
							<option value="21000">$21,000</option>
							<option value="22000">$22,000</option>
							<option value="23000">$23,000</option>
							<option value="24000">$24,000</option>
							<option value="25000">$25,000</option>
							<option value="26000">$26,000</option>
							<option value="27000">$27,000</option>
							<option value="28000">$28,000</option>
							<option value="29000">$29,000</option>
							<option value="30000">$30,000</option>
							<option value="31000">$31,000</option>
							<option value="32000">$32,000</option>
							<option value="33000">$33,000</option>
							<option value="34000">$34,000</option>
							<option value="35000">$35,000</option>
							<option value="36000">$36,000</option>
							<option value="37000">$37,000</option>
							<option value="38000">$38,000</option>
							<option value="39000">$39,000</option>
							<option value="40000">$40,000</option>
							<option value="41000">$41,000</option>
							<option value="42000">$42,000</option>
							<option value="43000">$43,000</option>
							<option value="44000">$44,000</option>
							<option value="45000">$45,000</option>
							<option value="46000">$46,000</option>
							<option value="47000">$47,000</option>
							<option value="48000">$48,000</option>
							<option value="49000">$49,000</option>
							<option value="50000">$50,000</option>
							<option value="51000">$51,000</option>
							<option value="52000">$52,000</option>
							<option value="53000">$53,000</option>
							<option value="54000">$54,000</option>
							<option value="55000">$55,000</option>
							<option value="56000">$56,000</option>
							<option value="57000">$57,000</option>
							<option value="58000">$58,000</option>
							<option value="59000">$59,000</option>
							<option value="60000">$60,000</option>
							<option value="61000">$61,000</option>
							<option value="62000">$62,000</option>
							<option value="63000">$63,000</option>
							<option value="64000">$64,000</option>
							<option value="65000">$65,000</option>
							<option value="66000">$66,000</option>
							<option value="67000">$67,000</option>
							<option value="68000">$68,000</option>
							<option value="69000">$69,000</option>
							<option value="70000">$70,000</option>
							<option value="71000">$71,000</option>
							<option value="73000">$73,000</option>
							<option value="74000">$74,000</option>
							<option value="75000">$75,000</option>
							<option value="76000">$76,000</option>
							<option value="78000">$78,000</option>
							<option value="79000">$79,000</option>
							<option value="80000">$80,000</option>
							<option value="81000">$81,000</option>
							<option value="82000">$82,000</option>
							<option value="83000">$83,000</option>
							<option value="84000">$84,000</option>
							<option value="85000">$85,000</option>
							<option value="87000">$87,000</option>
							<option value="88000">$88,000</option>
							<option value="89000">$89,000</option>
							<option value="90000">$90,000</option>
							<option value="91000">$91,000</option>
							<option value="93000">$93,000</option>
							<option value="94000">$94,000</option>
							<option value="95000">$95,000</option>
							<option value="96000">$96,000</option>
							<option value="97000">$97,000</option>
							<option value="98000">$98,000</option>
							<option value="99000">$99,000</option>
							<option value="100000">$100,000</option>
							<option value="102000">$102,000</option>
							<option value="104000">$104,000</option>
							<option value="106000">$106,000</option>
							<option value="107000">$107,000</option>
							<option value="109000">$109,000</option>
							<option value="110000">$110,000</option>
							<option value="113000">$113,000</option>
							<option value="114000">$114,000</option>
							<option value="115000">$115,000</option>
							<option value="116000">$116,000</option>
							<option value="119000">$119,000</option>
							<option value="120000">$120,000</option>
							<option value="121000">$121,000</option>
							<option value="124000">$124,000</option>
							<option value="125000">$125,000</option>
							<option value="127000">$127,000</option>
							<option value="129000">$129,000</option>
							<option value="130000">$130,000</option>
							<option value="134000">$134,000</option>
							<option value="135000">$135,000</option>
							<option value="148000">$148,000</option>
							<option value="153000">$153,000</option>
							<option value="155000">$155,000</option>
							<option value="157000">$157,000</option>
							<option value="159000">$159,000</option>
							<option value="161000">$161,000</option>
							<option value="173000">$173,000</option>
							<option value="174000">$174,000</option>
							<option value="175000">$175,000</option>
							<option value="184000">$184,000</option>
							<option value="185000">$185,000</option>
							<option value="189000">$189,000</option>
							<option value="199000">$199,000</option>
							<option value="223000">$223,000</option>
							<option value="228000">$228,000</option>
							<option value="240000">$240,000</option>
							<option value="245000">$245,000</option>
							<option value="249000">$249,000</option>
							<option value="250000">$250,000</option>
							<option value="270000">$270,000</option>
							<option value="280000">$280,000</option>
							<option value="290000">$290,000</option>
							<option value="300000">$300,000</option>
							<option value="320000">$320,000</option>
							<option value="340000">$340,000</option>
							<option value="349000">$349,000</option>
							<option value="350000">$350,000</option>
							<option value="390000">$390,000</option>
							<option value="400000">$400,000</option>
							<option value="401000">$401,000</option>
							<option value="410000">$410,000</option>
							<option value="420000">$420,000</option>
							<option value="437000">$437,000</option>
							<option value="493000">$493,000</option>
							<option value="600000">$600,000</option>
							<option value="750000">$750,000</option>
							<option value="800000">$800,000</option>
							<option value="889000">$889,000</option>
							<option value="999000">$999,000</option>
							<option value="1000000">$1,000,000</option>
						</select>
					</div>
				</div>

				<div class="col-xl-12">
					<div class="submit-field">
						<h5>Select Vehicle Status</h5>
                  </div>
                </div>
                
				<div class="col-xl-4"> 
                    <div class="checkbox">
                      <input name="vehicle_status[]" type="checkbox" value="1" id="new" checked>
                      <label class="padding-left-30" for="new"><span class="checkbox-icon"></span> New</label>
                    </div> 
                </div>
                
				<div class="col-xl-4"> 
                    <div class="checkbox">
                      <input name="vehicle_status[]" type="checkbox" value="2" id="used" checked>
                      <label class="padding-left-30" for="used"><span class="checkbox-icon"></span> Used</label>
                    </div> 
                </div>
                
				<div class="col-xl-4"> 
                    <div class="checkbox">
                      <input name="vehicle_status[]" type="checkbox" value="3" id="cert-used" checked>
                      <label class="padding-left-30" for="cert-used"><span class="checkbox-icon"></span> Certified Used</label>
                    </div> 
                </div>
                
				<div class="col-xl-12">
                  <div class="section-headline ">
                    <h5>Do yo have a car you want to trade?</h5>
                  </div>
                  <div class="radio">
                    <input id="no" name="trade_in" value="0" type="radio" onclick="$('#trade-in').addClass('hidden');" checked>
                    <label for="no"><span class="radio-label"></span> No</label>
                  </div> 
                  <div class="radio">
                    <input id="yes" name="trade_in" value="1" type="radio" onclick="$('#trade-in').removeClass('hidden');">
                    <label for="yes"><span class="radio-label"></span> Yes</label>
                  </div>
                </div>

				<!----- Show when Trade In is Yes ----->
				<div id="trade-in" class="col-xl-12 hidden margin-top-50">
					<div class="text-center"><h3>Please input your trade in details:</h3></div>
					<div class="col-xl-12">
						<div class="submit-field">
							<h5>Select Make</h5>
							<select name="trade_make" id="trade_make" class="selectpicker with-border" data-size="7" title="Make">
							<?php foreach($make as $key => $val):?>
								<option value="<?php echo $val->make?>"> <?php echo $val->make?></option> 
							<?php endforeach?>
							</select>
						</div>
					</div>

					<div class="col-xl-12">
						<div class="submit-field">
							<h5>Select Model</h5>
							<select name="trade_model" id="trade_model" class="selectpicker with-border" data-size="7" title="Model">
                    			<option>Please Select Make</option>
							</select>
						</div>
					</div>
 
					<div class="col-xl-12">
						<div class="submit-field">
							<h5>Select Trim</h5>
							<select name="trade_trim" id="trade_trim" class="selectpicker with-border" data-size="7" title="Trim">
								<option>Please Select Model</option>
								
							</select>
						</div>
					</div>
 
					<div class="col-xl-6">
						<div class="submit-field">
							<h5>Select Year</h5>
							<select name="trade_year" class="selectpicker with-border" data-size="7" title="From">
								<?php foreach($year as $key): ?>
									<option value="<?php echo $key->year?>"><?php echo $key->year ?></option> 
								<?php endforeach?>
							</select>
						</div>
					</div>

					<div class="col-xl-6">
						<div class="submit-field">
						<h5>Price</h5>
						<input type="text" class="input-text with-border" name="trade_price" id="trade-price" placeholder="Price" />
						</div>
					</div>

					
					<div class="col-xl-6">
						<div class="submit-field">
							<h5>Internal Condition</h5>
							<select name="trade_internal" class="selectpicker with-border" data-size="7" title="From"> 
								<option value="Poor">Poor</option>  
								<option value="Fair">Fair</option>  
								<option value="Good">Good</option>  
								<option value="Excellent">Excellent</option>  
								<option value="New">New</option>  
							</select>
						</div>
					</div>

					
					<div class="col-xl-6">
						<div class="submit-field">
							<h5>External Condition</h5>
							<select name="trade_external" class="selectpicker with-border" data-size="7" title="From"> 
								<option value="Poor">Poor</option>  
								<option value="Fair">Fair</option>  
								<option value="Good">Good</option>  
								<option value="Excellent">Excellent</option>  
								<option value="New">New</option>  
							</select>
						</div>
					</div>

				</div>
				

                 <!-- Button -->
                <div class="col-xl-12 margin-top-30 margin-bottom-30">
                 <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="vehicle-form">Continue <i class="icon-material-outline-arrow-right-alt"></i></button>
                </div>

              </div>
            </div>
            </form>

          </div>
				</div>
 
			</div>
			<!-- Row / End -->
 
		</div>
	</div>