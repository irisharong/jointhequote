<div class="container col-xl-4 padding-top-50">
   
    <!-- Welcome Text -->
    <div class="welcome-text">
        <h3>Login</h3>
        
    </div> 

    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

    <!-- Form -->
    <form method="post" id="login-form" action="<?php echo base_url('user/login'); ?>">
        <div class="input-with-icon-left">
            <i class="icon-material-baseline-mail-outline"></i>
            <input type="text" class="input-text with-border" name="email" id="email" placeholder="Email Address" required/>
        </div>

        <div class="input-with-icon-left">
            <i class="icon-material-outline-lock"></i>
            <input type="password" class="input-text with-border" name="password" id="password" placeholder="Password" required/>
        </div>

        <!-- Button -->
        <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="login-form">Log In <i class="icon-material-outline-arrow-right-alt"></i></button>
        
        <div class="pull-left margin-bottom-20 margin-top-20">
            <a href="<?php echo base_url('user/register') ?>" >Create Account</a>                                       
        </div>

        <div class="pull-right margin-bottom-20 margin-top-20">
            <a href="<?php echo base_url('user/reset') ?>" >Forgot Password?</a>                                       
        </div>
        
    </form>
     
    
</div>