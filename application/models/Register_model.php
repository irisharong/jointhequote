<?php

class Register_model extends CI_Model {
    function check_id($id){
        $q = $this->db->where('email',$id)
                      ->get('user');
        if($q->num_rows() > 0){
            return true;   
        }else{
            return false;   
        }
    }
    
    function check_email($email){
        $q = $this->db->where('email',$email)
                      ->get('user');
        if($q->num_rows() > 0){
            return true;   
        }else{
            return false;   
        }
    }
    
    function add_user(){
        $data = array(
                'firstname' => $this->input->post('firstname'),
                'lastname' => $this->input->post('lastname'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'password' => $this->input->post('password'),
                'level' => 'user',
                'status' => 1,
                'date_created' => date('Y-m-d G:i:s'),
            );
        $q = $this->db->where($data)
                      ->get('user');
        if($q->num_rows() > 0){
            echo 'duplicate';   
            return 0;
        }else{
            $this->db->insert('user',$data);  

            //add user to session
            $data = array(
                'user_id' => $this->db->insert_id(),
                'level' => $data['level'],
                'is_logged_in' => true
            );                           
            $this->session->set_userdata($data);                   

            return $this->db->insert_id();
        }
    }

    function add_member($dealer_id){
        $data = array(
            'firstname' => $this->input->post('firstname'),
            'lastname' => $this->input->post('lastname'),
            'email' => $this->input->post('email'),
            'phone' => $this->input->post('phone'),
            'password' => $this->input->post('password'),
            'level' => 'salesrep',
            'status' => 1,
            'dealer_id' => $dealer_id,
            'date_created' => date('Y-m-d G:i:s'),
        );
        $q = $this->db->where($data)
                        ->get('user');
        if($q->num_rows() > 0){
            echo 'duplicate';   
        }else{
            $this->db->insert('user',$data);  
        }
    }

    function add_customer_request($data){
                
        $this->db->insert('customer_request',$data);  
        return $this->db->insert_id();
        
    }

    function add_trade_request($trade){
        $this->db->insert('trade_in_details',$trade);
    }

    function get_trade_in_details($cr_id){
        $q = 'SELECT * 
              FROM trade_in_details
              WHERE customer_request_id = '.$cr_id ;

        $q = $this->db->query($q)->result();
        
        return $q;
    }

    function update_customer_request($data, $user_id){
         
        $this->db->where('user_id', $user_id);
        $this->db->update('customer_request', $data); 
        return true;
    }

    function update_customer_request_byId($data, $request_id){

        $this->db->where('id', $request_id);
        $this->db->update('customer_request', $data); 
        return true;
    }
 
}
?>