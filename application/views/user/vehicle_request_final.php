<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="container col-xl-4 margin-bottom-30">
					<div class="dashboard-box margin-top-0">
 
            <div class="welcome-text with-padding padding-top-30">
              <h3>Great! Last Step!</h3>
            </div> 
						<div class="container col-xl-10">
							<div class="submit-field">									
								<p>In order to provide you with quotes close to you, please tell us where you are looking for a car.</p>   
							</div>
						</div>


            <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

            <!-- Form -->
            <form method="post" id="vehicle-form-final-2" action="<?php echo base_url("user/request_last/$user_id/$cr_user_id"); ?>">
            <div class="content with-padding padding-bottom-10">
              <div class="row">
                
								<div class="col-xl-12">
									<div class="submit-field">									
									<h5>Zip Code</h5>
									<input type="text" class="input-text with-border" name="zipcode" id="zipcode" placeholder="Zip Code" required/>
									</div>
								</div>

								<div class="col-xl-12">
									<div class="submit-field">
										<h5>Miles From</h5>
										<select class="selectpicker with-border" id="vehicle_miles" name="miles" data-size="5" title="Select One">
											<option>Please Input Zip Code</option>
										</select>
									</div>
								</div>
  
                 <!-- Button -->
                <div class="col-xl-12 margin-top-30 margin-bottom-30">
                 <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="vehicle-form-final-2" name="finish" id="finish">Finish <i class="icon-material-outline-arrow-right-alt"></i></button>
								</div> 
								
              </div>
            </div>
            </form>

          </div>
				</div>
 
			</div>
			<!-- Row / End -->
 
		</div>
	</div>