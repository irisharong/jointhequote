<?php

class Message_model extends CI_Model {
    public $total_unread;
    public $total_sent;
    public $total_trash;
	function get_contacts(){
        $id = $this->session->userdata('user_id');
        $q = $this->db
                ->where('id!=',$id)
                ->get('user')->result();
        return $q;
    }

    function send_message(){
        
        $data = array(
                'date' => date('Y-m-d G:i:s'),
                'user_from' => $this->session->userdata('user_id'),
                'user_to' => $this->input->post('user_to'), 
                'content' => $this->input->post('textbox'), 
                'status' => '0' //unread
            );        
        $this->db->insert('message',$data); 
        return TRUE;
    }
    
    function get_firstname($id){
       $q = $this->db
                    ->where('id',$id)
                    ->get('user')
                    ->result();
        foreach($q as $row){
            return $row->firstname.' '.$row->lastname;   
        }
    }
    function get_all_messages(){
        $q = $this->db
                    ->where('location','inbox')
                    ->order_by('date','desc')
                    ->get('message')                    
                    ->result();
        return $q;
    }
    
    function get_messages(){
        $id = $this->session->userdata('user_id');
        $where = "user_to=$id OR user_from=$id";
        
        $q = $this->db
                    ->where($where)
                    ->order_by('date','asc')
                    ->get('message')                    
                    ->result();
        return $q;
    }

    function get_messages_thread($user_to, $user_from){
        $where = "user_to IN ($user_to, $user_from) AND  user_from IN ($user_to, $user_from)";
        
        $q = $this->db
                    ->where($where)
                    ->order_by('date','asc')
                    ->get('message')                    
                    ->result();
        return $q;
    }
    
    function get_messages_by_search(){
        $id = $this->session->userdata('user_id');
        $keyword = $this->input->post('keyword');
        
        $q = 'SELECT user.email,DATE_FORMAT(message.date,"%b %d %Y %h:%i %p") as date,message.content,message.message_id,message.subject,message.user_from,message.status
            FROM message
            LEFT JOIN user on message.user_from = user.user_id
            WHERE message.user_to='.$id.'
            AND (
                message.subject LIKE "%'.$keyword.'%"
                OR message.content LIKE "%'.$keyword.'%"
                OR user.email LIKE "%'.$keyword.'%"
            )
            ORDER BY message.date DESC
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }
    
    function get_all_messages_by_search(){
        $id = $this->session->userdata('user_id');
        $keyword = $this->input->post('keyword');
        
        $q = 'SELECT user.email,DATE_FORMAT(message.date,"%b %d %Y %h:%i %p") as date,message.content,message.message_id,message.subject,tbl_message.user_from,tbl_message.user_to,tbl_message.status
            FROM message
            LEFT JOIN user on message.user_from = user.user_id
            WHERE message.subject LIKE "%'.$keyword.'%"
                OR message.content LIKE "%'.$keyword.'%"
                OR user.email LIKE "%'.$keyword.'%"
            ORDER BY message.date DESC
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }
    
    function get_messages_sent_by_search(){
        $id = $this->session->userdata('user_id');
        $keyword = $this->input->post('keyword');
        
        $q = 'SELECT user.email,DATE_FORMAT(message_sent.date,"%b %d %Y %h:%i %p") as date,message_sent.content,message_sent.message_id,message_sent.subject,message_sent.user_to,message_sent.status
            FROM message_sent
            LEFT JOIN user on message_sent.user_to = user.id
            WHERE message_sent.user_from='.$id.'
            AND (
                message_sent.subject LIKE "%'.$keyword.'%"
                OR message_sent.content LIKE "%'.$keyword.'%"
                OR user.email LIKE "%'.$keyword.'%"
            )
            ORDER BY message_sent.date DESC
        ';
        $q = $this->db->query($q)->result();
        return $q;
    }
    
    function get_messages_sent(){
        $id = $this->session->userdata('user_id');
        $q = $this->db
                    ->where('user_from',$id)
                    ->order_by('status,date','desc')
                    ->get('message_sent')                    
                    ->result();
        return $q;
    }
    
    function get_messages_trash(){
        $id = $this->session->userdata('user_id');
        $q = $this->db
                    ->where('user_to',$id)
                    ->where('location','trash')
                    ->order_by('status,date','desc')
                    ->get('message')                    
                    ->result();
        return $q;
    }
    
    function get_message_by_id($message_id,$location=null){
        $id = $this->session->userdata('user_id');
        $this->db->select('user.email,DATE_FORMAT(message.date,"%b %d %Y %h:%i %p") as date,message.content,message.message_id,message.subject,message.user_from');
        $this->db->where('message_id',$message_id);
        $this->db->join('user','user.user_id=message.user_from','left');        
        $q = $this->db->get('message')->result();               
        if($location!=null){
            $this->read_message($message_id);
        }
        
        return $q;
    }
    
    function get_admin_message_by_id($message_id){
        $id = $this->session->userdata('user_id');
        $this->db->select('user.email,DATE_FORMAT(message.date,"%b %d %Y %h:%i %p") as date,message.content,message.message_id,message.subject,message.user_from,message.user_to');
        $this->db->where('message_id',$message_id);
        $this->db->join('user','user.user_id=message.user_from','left');        
        $q = $this->db->get('message')->result();               

        return $q;
    }
    
    function get_message_sent_by_id($message_id){
        $id = $this->session->userdata('user_id');
        $this->db->select('user.email,DATE_FORMAT(message.date,"%b %d %Y %h:%i %p") as date,message.content,message.message_id,message.subject,message.user_to');
        $this->db->where('message_id',$message_id);
        $this->db->join('user','user.user_id=message.user_to','left');        
        $q = $this->db->get('message')->result();               

        return $q;
    }
    
    function read_message($message_id){
        $rec = array(
               'status' => 'read'
            );
        
        $this->db->where('message_id', $message_id);
        $this->db->update('message', $rec); 
        return true;
        
    }
    
    function update_message(){
        $post = $this->input->post();
        $data = array(
                'subject' => $post['subject'],
                'content' => $post['content'],
                'status' => 'unread'
            );
        $message_id = $post['message_id'];
        $this->db->where('message_id', $message_id);
        $this->db->update('message', $data); 
        return true;
    }
    
    function count_messages(){
        $id = $this->session->userdata('user_id');
        $q = "select * from message where user_to=$id and status='unread'";
        $rs = $this->db->query($q);
        $this->total_unread = $rs->num_rows();
        
        $q = "select * from message_sent where user_from=$id";
        $rs = $this->db->query($q);
        $this->total_sent = $rs->num_rows();
        
        $q = "select * from message where location='trash' and user_to=$id";
        $rs = $this->db->query($q);
        $this->total_trash = $rs->num_rows();
    }

    
    function delete_message(){
        $ids = $this->input->post('message_id');
        $c = count($ids);
        if($c == 0){
            $ids = array($this->uri->segment(3));   
        }
        $c = count($ids);
        for($i=0; $i < $c; $i++){
            $message_id = $ids[$i];   
            echo $message_id;
            $this->db
                    ->where('message_id',$message_id)
                    ->delete('message'); 
        }
        
    }
    
    function delete_message_sent(){
        $ids = $this->input->post('message_id');
        $c = count($ids);
        if($c == 0){
            $ids = array($this->uri->segment(3));   
        }
        $c = count($ids);
        for($i=0; $i < $c; $i++){
            $message_id = $ids[$i];   
            $this->db
                    ->where('message_id',$message_id)
                    ->delete('message_sent'); 
        }
        
    }
        
    
    function string_limit_words($string, $word_limit) { 
        $words = explode(' ', $string); 
        return implode(' ', array_slice($words, 0, $word_limit)); 
    }
    
    function time_diff($date_in){
            $start_date = $date_in;
            $end_date=date('Y-m-d H:i:s');

            $start_time = strtotime($start_date);
            $end_time = strtotime($end_date);
            $difference = $end_time - $start_time;

            $seconds = $difference % 60;            //seconds
            $difference = floor($difference / 60);

            $min = $difference % 60;              // min
            $difference = floor($difference / 60);

            $hours = $difference % 24;  //hours
            $difference = floor($difference / 24);

            $days = $difference % 30;  //days
            $difference = floor($difference / 30);

            $month = $difference % 12;  //month
            $difference = floor($difference / 12);
            
            $year = $difference % 1;  //month
            $difference = floor($difference / 1);


            $result = null;
            if($year!=0) {                
                if($year == 1){
                    $result.=$year.' Year ';    
                }else{
                    $result.=$year.' Years ';   
                }
            }
            if($month!=0) {                
                if($month == 1){
                    $result.=$month.' Month ';    
                }else{
                    $result.=$month.' Months ';   
                }
            }
            if($days!=0) {                
                if($days == 1){
                    $result.=$days.' Day ';    
                }else{
                    $result.=$days.' Days ';   
                }
            }
            if($hours!=0) {                
                if($hours == 1){
                    $result.=$hours.' Hour ';    
                }else{
                    $result.=$hours.' Hours ';   
                }
            }
            if($min!=0) {                
                if($min == 1){
                    $result.=$min.' Minute ';    
                }else{
                    $result.=$min.' Minutes ';   
                }
            }
            
            if($result==null){
                return 'Just Now';   
            }
            return $result.' ago';
        }
}
?>