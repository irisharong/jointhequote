

<!-- Page Content
================================================== -->
<div class="container customer-dashboard">
	<div class="row">
		
		<div class="col-xl-12 col-lg-12 content-left-offset">
 
    <div class="section-headline margin-top-30 margin-bottom-30">
      <h4>Customer Requests</h4>
    </div>

			<div class="tabs">
        <div class="tabs-header">
					<ul>
						<li class="active"><a href="#tab-1" data-tab-id="1">1st Quote</a></li>
						<li><a href="#tab-2" data-tab-id="2">2nd Quote</a></li>
						<li><a href="#tab-3" data-tab-id="3">Appointment</a></li>
					</ul>
					<div class="tab-hover"></div>
					<nav class="tabs-nav">
						<span class="tab-prev"><i class="icon-material-outline-keyboard-arrow-left"></i></span>
						<span class="tab-next"><i class="icon-material-outline-keyboard-arrow-right"></i></span>
					</nav>
				</div>

        <div class="tabs-content">
					<div class="tab active" data-tab-id="1">
						   <!-- Dashboard Content
                ================================================== -->
                <div class="dashboard-content-container" data-simplebar>
                  <div class="" >
                    
                    <div class="container">
                      <div class="row">
                      
                      <div class="notify-box margin-bottom-20">
                        
                        <div class="sort-by">
                          <span>Filter by:</span>
                          <select class="selectpicker hide-tick">
                            <option>All</option>
                            <option>Customer</option>
                            <option>Model</option>
                            <option>Status</option>
                          </select>
                        </div>
                      </div>
          
                        <div class="col-xl-12 col-md-12">

                          <table id="customer-request" class="basic-table quote-table">
                            <tr>
                              <th>Customer</th>  
                              <th>Time Left</th>
                              <th>Customer Vehicle</th>
                              <th>Has Trade In</th>
                              <th>Dealer Quotes</th>
                              <th>Status</th> 
                            </tr>
                          <?php $cnt=0;
                            $end_quote = array(); 
                            $end = 0;

                          if(!empty($customer_request)):
                          ?>        
                          <?php foreach($customer_request as $request): ?>  
                          <?php $cnt++; 
                            if(strtotime($request->request_end_time) < time()){
                              $end = 1;
                              //if($quote[$request->id]){
                                $end_quote[] = $request;
                                continue;
                             // } 
                            }
 

                          ?>
                            <tr>
                              <td> <?php echo $request->firstname." ".$request->lastname?> </td>  
                              <td>  
                                <?php if(strtotime($request->request_end_time) < time()){
                                      echo "00:00";
                                      $end = 1;
                                     
                                }else{
                                  $current = date('Y-m-d G:i:s');
                                  $request_end_time = new DateTime($request->request_end_time);
                                  $current_time = new DateTime($current);
                              
                                  $interval = $current_time->diff($request_end_time);
                                
                                  if($interval->format('%d') != 0){
                                    echo $interval->format('%d')."d ".$interval->format('%h')."h ".$interval->format('%i')."m";
                                  }else{
                                    echo $interval->format('%h')."h ".$interval->format('%i')."m";
                                  }
                                
                                }?>
                              </td>
                              <td> <strong><?php echo $request->make." ".$request->model?></strong><br> 
                                  <span>Year: <?php echo $request->year_from." - ".$request->year_to?><span><br>
                                  <span>Price: <?php $price_from = "";
                                                     $price_to = "";
                                                     if (!empty($request->price_from) || $request->price_from != null) {
                                                       $price_from = number_format($request->price_from);
                                                     }
                                                     if (!empty($request->price_to) || $request->price_to != null) {
                                                       $price_to = number_format($request->price_to);
                                                     }
                                                     echo "$".$price_from." - $".$price_to?></span>
                              </td>
                              <td><?php echo $trade = ($request->trade_in) ? "Yes" : "No" ?> </td>
                              <td><?php echo $quote_num[$request->id]?></td>
                              <?php if($quote[$request->id] != 0 ){?>
                              <td><span>Quote Submitted: <?php echo date("m/d/Y h:ia", strtotime($submitted_quote[$request->id])) ?> MDT</span> 
                                  <br><span>Sales Person: <?php echo $firstname ?></span><br>
                                  <a href="#small-dialog-<?php echo  $cnt; ?>" class="popup-with-zoom-anim"  onClick='loadMessage(<?php echo $request->user_id?>, <?php echo json_encode($user_initials)?>)' id="chatbox-<?php echo $request->user_id?>" data-id="<?php echo $request->user_id?>">
                                    <span><i class="icon-feather-message-square"></i>Messages</span>
                                  </a> 
                                  <form method="post" id="etq-form" action="<?php echo base_url('dealer/edit_quote'); ?>">
                                    <span><i class="icon-feather-edit"></i>
                                      <input type="submit" value="Edit Quote" class="input-link" id="input-link" />
                                      <input type="hidden" name="cr_id" value="<?php echo  $request->id; ?>" /> 
                                    </span> 
                                  </form>
                              </td>
                              <?php }else{ ?>
                              <td>
                                <?php if($end){ echo ""; }else{?>  
                                  <form method="post" id="jtq-form" action="<?php echo base_url('dealer/join_the_quote'); ?>">
                                    <input type="hidden" name="cr_id" value="<?php echo  $request->id; ?>" />
                                    <input type="submit" value="Join The Quote" class="button gray ripple-effect" />
                                  </form>
                                <?php } ?>
                                
                              <?php } ?>
                              </td>
                                <!-- Send Direct Message Popup
                                ================================================== -->
                                <div id="small-dialog-<?php echo  $cnt; ?>" class="small-dialog zoom-anim-dialog mfp-hide">
                                  <!--Tabs -->
                                  <div class="sign-in-form">
                                    <ul class="popup-tabs-nav">
                                      <li><a href="#tab">Send Message</a></li>
                                    </ul>
                                    <div class="popup-tabs-container">
                                      <!-- Tab -->
                                      <div class="popup-tab-content" id="tab">                              
                                        <div class="message-content">
                                          <div class="messages-headline">
                                            <h4><?php echo $request->firstname." ".$request->lastname?></h4> 
                                          </div>
                                          <!-- Message Content Inner -->
                                          <div id="message-content-<?php echo $request->user_id?>" class="message-content-inner">                                    
                                              <!-- Time Sign -->
                                              <div class="message-time-sign">                                      
                                              </div>
                                          </div>
                                          <!-- Message Content Inner / End -->
                                          <!-- Reply Area -->
                                          <div class="message-reply">
                                            <textarea id="textbox-<?php echo $request->user_id?>" cols="1" rows="1" placeholder="Your Message" data-autoresize></textarea>
                                            <button id="send" onClick='sendMessage(<?php echo $request->user_id?>, <?php echo $user_id?>, <?php echo json_encode($user_initials)?>)' class="button ripple-effect">Send</button>
                                          </div>
                                          </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Send Direct Message Popup / End -->

                            </tr> 
                            
                          <?php endforeach ?>
                          <?php endif ?>
                          </table>
                        </div> 
                      </div> 
                    </div> 
                  </div>
                </div>
                <!-- Dashboard Content / End -->
					</div>
					<div class="tab" data-tab-id="2">
            <table id="customer-request2" class="basic-table quote-table"> 
              <tr>
                <th>Customer</th>   
                <th>Customer Vehicle</th>
                <th>Has Trade In</th>
                <th>Dealer Quotes</th>
                <th>Status</th> 
              </tr>
              <?php foreach($end_quote as $request): ?>  
                <?php $cnt++; ?>
                  <tr>
                    <td> <?php echo $request->firstname." ".$request->lastname?> </td>  
                    
                    <td> <strong><?php echo $request->make." ".$request->model?></strong><br> 
                        <span>Year: <?php echo $request->year_from." - ".$request->year_to?><span><br>
                        <span>Price: <?php $price_from = "";
                                           $price_to = "";
                                           if (!empty($request->price_from) || $request->price_from != null) {
                                             $price_from = number_format($request->price_from);
                                           }
                                           if (!empty($request->price_to) || $request->price_to != null) {
                                             $price_to = number_format($request->price_to);
                                           }
                                           echo "$".$price_from." - $".$price_to?></span>
                    </td>
                    <td><?php echo $trade = ($request->trade_in) ? "Yes" : "No" ?> </td>
                    <td><?php echo $quote[$request->id]?></td>
                    <?php if($quote[$request->id] != 0 ){?>
                    <td><span>Quote Submitted: <?php echo date("m/d/Y h:ia", strtotime($submitted_quote[$request->id])) ?> MDT</span> 
                        <br><span>Sales Person: <?php echo $firstname ?></span><br>
                        <a href="#small-dialog-<?php echo  $cnt; ?>" class="popup-with-zoom-anim"  onClick='loadMessage(<?php echo $request->user_id?>, <?php echo json_encode($user_initials)?>)' id="chatbox-<?php echo $request->user_id?>" data-id="<?php echo $request->user_id?>">
                          <span><i class="icon-feather-message-square"></i>Messages</span>
                        </a>
                        <form method="post" id="etq-form" action="<?php echo base_url('dealer/edit_quote'); ?>">
                          <span><i class="icon-feather-edit"></i>
                            <input type="submit" value="Edit Quote" class="input-link" id="input-link" />
                            <input type="hidden" name="cr_id" value="<?php echo  $request->id; ?>" />                           
                          </span> 
                        </form>
                    </td>
                    <?php }else{ ?>
                    <td>
                      
                    <?php } ?>
                    </td>

                      <!-- Send Direct Message Popup
                      ================================================== -->
                      <div id="small-dialog-<?php echo  $cnt; ?>" class="small-dialog zoom-anim-dialog mfp-hide">
                        <!--Tabs -->
                        <div class="sign-in-form">
                          <ul class="popup-tabs-nav">
                            <li><a href="#tab">Send Message</a></li>
                          </ul>
                          <div class="popup-tabs-container">
                            <!-- Tab -->
                            <div class="popup-tab-content" id="tab">                              
                              <div class="message-content">
                                <div class="messages-headline">
                                  <h4><?php echo $request->firstname." ".$request->lastname?></h4> 
                                </div>
                                <!-- Message Content Inner -->
                                <div id="message-content-<?php echo $request->user_id?>" class="message-content-inner">                                    
                                    <!-- Time Sign -->
                                    <div class="message-time-sign">                                      
                                    </div>
                                </div>
                                <!-- Message Content Inner / End -->
                                <!-- Reply Area -->
                                <div class="message-reply">
                                  <textarea id="textbox-<?php echo $request->user_id?>" cols="1" rows="1" placeholder="Your Message" data-autoresize></textarea>
                                  <button id="send" onClick='sendMessage(<?php echo $request->user_id?>, <?php echo $user_id?>, <?php echo json_encode($user_initials)?>)' class="button ripple-effect">Send</button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <!-- Send Direct Message Popup / End -->

                  </tr>  
                <?php endforeach ?>
            </table>
					</div>
					<div class="tab" data-tab-id="3">
            <table id="customer-request3" class="basic-table quote-table"> 
              <tr>
                <th>Customer</th>   
                <th>Customer Contacts</th>
                <th>Customer Vehicle</th>
                <th>Has Trade In</th>
                <th>Appointment</th>
                <th>Your Quote</th> 
              </tr> 
              <?php foreach($appointments as $appointment): ?>  
              <?php $cnt++; ?>
              <tr>
                <td> <?php echo $appointment->firstname." ".$appointment->lastname?> </td>   
                <td><span>Email: <?php echo $appointment->email?><span><br>
                    <span>Phone number: <?php echo $appointment->phone?><span></td>                     
                <td><strong><?php echo $appointment->make." ".$appointment->model?></strong><br> 
                    <span>Year: <?php echo $appointment->year_from." - ".$appointment->year_to?><span><br>
                    <span>Mileage: <?php $mileage_from = "";
                                         $mileage_to = "";
                                         if (!empty($appointment->mileage_from) || $appointment->mileage_from != null) {
                                            $mileage_from = number_format($appointment->mileage_from);
                                         }
                                         if (!empty($appointment->mileage_to) || $appointment->mileage_to != null){
                                            $mileage_to = number_format($appointment->mileage_to);
                                         }	 
                                         echo $mileage_from." - ".$mileage_to?><span><br>

                    <span>Price: <?php  $price_from = "";
                                        $price_to = "";
                                        if (!empty($appointment->price_from) || $appointment->price_from != null) {
                                          $price_from = number_format($appointment->price_from);
                                        }
                                        if (!empty($appointment->price_to) || $appointment->price_to != null) {
                                          $price_to = number_format($appointment->price_to);
                                        }

                                        echo "$".$price_from." - $".$price_to?></span>
                </td>
                <td><?php echo $trade = ($appointment->trade_in) ? "Yes" : "No" ?> </td>
                <td><?php echo date("m/d/Y", strtotime($appointment->appointment_date))." @ ".date("h:i a",strtotime($appointment->appointment_time)) ?> </td>           
                <td><strong><?php echo $appointment->v_make." ".$appointment->v_model?></strong><br> 
                    <span>Year: <?php echo $appointment->v_year?><span><br>
                    <span>Mileage: <?php $mileage = "";
                                        if (!empty($appointment->v_mile) || $appointment->v_mile != null){ 
                                          $mileage = number_format($appointment->v_mile);
                                        } 
                                        echo $mileage?><span><br>
                    <span>Price: <?php $price = "";
                                       if (!empty($appointment->v_price) || $appointment->v_price != null) {
                                        $price = number_format($appointment->v_price);
                                       }
                              echo "$".$price?><span><br>
                    <?php if (!empty($appointment->trade_in_offer)){ ?> 
                      <span>Trade in Offer: <?php echo "$".number_format($appointment->trade_in_offer)?></span>
                    <?php } ?>   
                </td> 
              </tr> 
              <?php endforeach ?>              
            </table>
					</div>
				</div> 
      </div> <!-- End Tab ---> 
		</div>
	</div>
</div>
