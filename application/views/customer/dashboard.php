

<!-- Page Content
================================================== -->
<div class="container customer-dashboard">
	<div class="row"> 
    
			<?php if(isset($requestId) && $requestId !=""){ ?>
				<div class="request-success">
					<div class="notification success closeable">
						<p style="text-align:center;">Successfully added new request vehicle!</p>
						<a class="close" href="#"></a>
					</div>
				</div>
			<?php } ?>

    <div class="col-xl-12">    
      <div class="float-right margin-top-30"><a class="button dark ripple-effect" href="<?php echo base_url("user/request/$user_id")?>" ><i class="icon-line-awesome-car"></i> Request New Vehicle</a></div>
    </div>

		<div class="col-xl-12 col-lg-12 content-left-offset margin-bottom-50">
  
			<div class="listings-container compact-list-layout margin-top-10">
        <!-- set request id if new request created-->
        <div class="new-request" data-req-id="<?php echo $requestId; ?>"></div>
        <div class="tabs">
          <div class="tabs-header">
            <ul>  
              <?php $time_remaining = ""; ?>
              <?php foreach($request as $req):   
                  
                  if(strtotime($req->request_end_time) < time()){
                    $time_remaining[$req->id] = "00:00"; 
                      
                  }else{
                    $current = date('Y-m-d G:i:s');
                    $request_end_time = new DateTime($req->request_end_time);
                    $current_time = new DateTime($current);
                
                    $interval = $current_time->diff($request_end_time);
                  
                    if($interval->format('%d') != 0){
                      $time_remaining[$req->id] = $interval->format('%d')."d ".$interval->format('%h')."h ".$interval->format('%i')."m";
                    }else{
                      $time_remaining[$req->id] = $interval->format('%h')."h ".$interval->format('%i')."m";
                    }
                  
                  }
                  ?> 
                <li class="active tab-<?php echo $req->id ?>"><a href="#tab-<?php echo $req->id ?>" data-tab-id="<?php echo $req->id ?>" ><?php echo $req->model ?></a></li> 
                 
              <?php endforeach ?>
            </ul>
            <div class="tab-hover"></div>
            <nav class="tabs-nav">
              <span class="tab-prev"><i class="icon-material-outline-keyboard-arrow-left"></i></span>
              <span class="tab-next"><i class="icon-material-outline-keyboard-arrow-right"></i></span>
            </nav>
          </div>
          <!-- Tab Content -->          
         
          <div class="tabs-content " id="tabcontent">
            <?php foreach($quote_result as $quote => $value): ?> 
              <div class="tab active tab-<?php echo $quote ?>" data-tab-id="<?php echo $quote ?>">  
 
                <?php if(!empty($value)){ 
                     // $time_requested = strtotime($value['requested_time']);
                     // $default_time_start = strtotime(date())
                     // var_dump( $time_requested);
                  ?>


                    <div class="sort-by">
                      <span>Sort by:</span>
                      <select class="selectpicker hide-tick">
                        <option>Relevance</option>
                        <option>Newest</option>
                        <option>Oldest</option>
                        <option>Random</option>
                      </select>
                    </div>

                    <div class=" padding-top-30 text-center">
                      <div class=" display-inline-block col-md-12 text-center">Time Remaining: <?php echo $time_remaining[$quote]; ?></div>

                    </div>  
                    <div class="padding-bottom-30 padding-top-30">
                      <div class=" display-inline-block col-md-8">Vehicle Details</div>
                      <div class="display-inline-block col-md-2">Dealer Info</div>
                      <div class="display-inline-block col-md-1 text-center">Action</div>

                    </div>  
                    <?php $cnt=0 ?>        
                  <?php foreach($value as $q): 
                    //var_dump($q);
                   // if(!is_object($q)) continue; ?>
                    <?php $cnt++; ?>
                    <!-- Job Listing -->
                    <div class="job-listing">
                  
                      <!-- Job Listing Details -->
                      <div class="display-inline-block col-md-8">
                      <a href="#small-dialog-<?php echo $q->id?>" class="popup-with-zoom-anim" id="vehicle-<?php echo $q->id?>" data-id="<?php echo $q->id?>">
                          
                        <div class="job-listing-details">
                          <!-- Logo -->
                          <div class="job-listing-company-logo">                          
                            <img src="<?php echo $q->image_url?>" alt="">
                          </div>

                          <!-- Details -->
                          <div class="job-listing-description">
                            <h3 class="job-listing-title"><?php echo $q->make." ".$q->model." ".$q->trim ?></h3>

                            <!-- Job Listing Footer -->
                            <div class="job-listing-footer">
                              <ul>
                                <li> Mileage: <?php echo $q->mileage?></li>
                                <li> Interior: <?php echo $q->interior_color?></li>
                                <li>Tow Package</li>
                              </ul>
                            </div>
                            <div class="job-listing-footer">
                              <ul>
                                <li> <button class="button gray">Price: $<?php echo $q->price?></button></li>
                                <!--<li><button class="button gray">Trade in: </button></li>-->
                                <li><button class="button gray">Add On: Oil Changes</button></li>
                              </ul>
                            </div>
                          </div>
                          <!-- Bookmark -->
                          <span class="bookmark-icon"></span>
                        </div> 
                          
                          </a>
                      </div>

                      <div class="display-inline-block col-md-2">
                          <!-- Logo -->
                        <div class="dealer-logo">
                          <img src="<?php echo $q->vehicle_url?>" alt="">
                        </div>
                        <div class="job-listing-footer">
                            <ul>
                              <li> <button class="gray">Contact <?php echo $q->firstname." ".$q->lastname ?></button></li>
                            </ul>
                        </div>
                      </div>

                      <div class="display-inline-block col-md-1 text-center">
                            
                        <!-- Notifications -->
                        <div class="header-notifications">
                          <!-- Trigger -->
                          <div class="header-notifications-trigger">
                            <a href="#"><i class="icon-line-awesome-ellipsis-h action-button action-dropdown"></i></a>
                          </div>
                          <!-- Dropdown -->
                          <div id="notif-dropdown" class="header-notifications-dropdown">

                            <div class="header-notifications-content">
                              <div class="header-notifications-scroll" style="height: 100.2px !important;" data-simplebar>
                                <ul>
                                  <!-- Notification -->
                                  <li class="notifications-not-read">
                                    <a href="#small-dialog-<?php echo  $cnt; ?>" class="popup-with-zoom-anim chatbox" onClick='loadMessage(<?php echo $q->dealer_id?>, <?php echo json_encode($user_initials)?>)' id="chatbox-<?php echo $q->dealer_id?>" data-id="<?php echo $q->dealer_id?>">
                                      <span class="notification-text">
                                        Message Dealer
                                      </span>
                                    </a>
                                  </li>

                                  <!-- Notification -->
                                  <li>
                                    <a href="#small-dialog-sched<?php echo  $cnt; ?>" class="popup-with-zoom-anim" onClick="" id="scheduler-<?php echo $q->dealer_id?>" data-id="<?php echo $q->dealer_id?>">
                                      <span class="notification-text">
                                        Schedule Test Drive
                                      </span>
                                    </a>
                                  </li>

                                  <!-- Notification -->
                                  <li>
                                    <a onClick=" return confirm('Are you sure you want to delete?'); " href="<?php echo base_url('customer/remove_quote/'.$q->quote_id); ?>">
                                      <span class="notification-text">
                                        Remove Quote
                                      </span>
                                    </a>
                                  </li>

                                </ul>


                                <!-- Send Direct Message Popup
                                ================================================== -->
                                <div id="small-dialog-<?php echo  $cnt; ?>" class="small-dialog zoom-anim-dialog mfp-hide">
                                  <!--Tabs -->
                                  <div class="sign-in-form">
                                    <ul class="popup-tabs-nav">
                                      <li><a href="#tab">Send Message</a></li>
                                    </ul>
                                    <div class="popup-tabs-container">
                                      <div class="popup-tab-content" id="tab">                                        
                                        <div class="message-content">
                                          <div class="messages-headline">
                                            <h4><?php echo $q->firstname." ".$q->lastname ?></h4> 
                                          </div>
                                          <!-- Message Content Inner -->
                                          <div id="message-content-<?php echo $q->dealer_id?>" class="message-content-inner">                                              
                                              <!-- Time Sign -->
                                              <div class="message-time-sign">
                                                
                                              </div>
                                            
                                          </div>
                                          <!-- Message Content Inner / End -->

                                          <!-- Reply Area -->
                                          <div class="message-reply">
                                            <textarea id="textbox-<?php echo $q->dealer_id?>" cols="1" rows="1" placeholder="Your Message" data-autoresize></textarea>
                                            <button id="send" onClick='sendMessage(<?php echo $q->dealer_id?>, <?php echo $user_id?>, <?php echo json_encode($user_initials)?>)' class="button ripple-effect">Send</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- Send Direct Message Popup / End -->


                                
                                <!-- Schedule Appointment
                                ================================================== -->
                                <div id="small-dialog-sched<?php echo  $cnt; ?>" class="small-dialog-sched small-dialog zoom-anim-dialog mfp-hide">
                                  <!--Tabs -->
                                  <div class="sign-in-form">
                                    <ul class="popup-tabs-nav"> 
                                    </ul>
                                    <div class="popup-tabs-container">
                                          
                                      <div class="popup-tab-content" id="tab">    
                                        <div class="welcome-text">
                                          <h3>Schedule Appointment</h3>
                                          <span>Enter your info below and select a time for you to test drive this vehicle. </span>
                                        </div>                          

                                        <div class="message-content">   
                                                          
                                          <!-- Dashboard Box -->
                                          <div class="container col-xl-12 margin-bottom-30">
                                            <!-- Form -->
                                            <form method="post" id="appointment-form" action="<?php echo base_url('customer/schedule_appointment'); ?>">
                                              <div class="dashboard-box margin-top-0">
                                          
                                                <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

                                                <div class="content with-padding padding-bottom-10">
                                                  <div class="row">

                                                    <div class="col-xl-6">
                                                      <div class="submit-field">
                                                      <h5>First Name</h5>
                                                      <input type="text" class="input-text with-border" name="firstname" id="firstname" placeholder="First Name" value="<?php echo $firstname?>" required readonly />
                                                      </div>
                                                    </div>

                                                    <div class="col-xl-6">
                                                      <div class="submit-field">
                                                      <h5>Last Name</h5>
                                                      <input type="text" class="input-text with-border" name="lastname" id="lastname" placeholder="Last Name" value="<?php echo $lastname?>" required readonly/>
                                                      </div>
                                                    </div>
                                                    
                                                    <div class="col-xl-6">
                                                      <div class="submit-field">
                                                      <h5>Email</h5>
                                                      <input type="text" class="input-text with-border" name="email" id="email" placeholder="Email" value="<?php echo $email?>" required readonly/>
                                                      </div>
                                                    </div>

                                                    <div class="col-xl-6">
                                                      <div class="submit-field">
                                                      <h5>Phone</h5>
                                                      <input type="text" class="input-text with-border" name="phone" id="phone" placeholder="Phone" value="<?php echo $phone?>" required readonly/>
                                                      </div>
                                                    </div>

                                                    <div class="col-xl-6">
                                                      <div class="submit-field">                                                    
                                                      <h5>Choose a Date</h5>
                                                      <?php
                                                        $date = date("Y-m-d");
                                                        $onload_date = strtotime($date);
                                                        $onload_time = date("H:i");
                                                        $box_day = date('l', $onload_date);
                                                        $box_date = date('F dS Y', $onload_date);
                                                        $box_time = date("g:i a");
                                                      ?>
                                                      <input type="date" class="input-text with-border" name="date" id="date" value="<?php echo $date;?>" required/>
                                                      </div>
                                                    </div>

                                                    <div class="col-xl-6">
                                                      <div class="submit-field">                                                    
                                                      <h5>Choose a Time</h5>
                                                      <input type="time" class="input-text with-border" name="time" id="time" value="<?php echo $onload_time;?>" placeholder="time" required/>
                                                      </div>
                                                    </div>
                                                    
                                                    <div class="col-xl-12">
                                                      <div class="submit-field" >  
                                                        <div class="appointment-table" >  
                                                          <div class="padding-top-15 display-inline-block col-md-12 text-center  ">Your appointment would be scheduled for:</div>
                                                          <div name="datetime" id="datetime" class=" padding-bottom-15 display-inline-block col-md-12 text-center inline-text-strong"><?php echo $box_day.", ".$box_date." @ " .$box_time." MST";?></div>
                                                          <div class="padding-top-15 display-inline-block col-md-12 text-center">At the following location:</div>
                                                          <div name="dealership-name" id="dealership-name" class=" display-inline-block col-md-12 text-center inline-text-strong "> <?php echo $q->dealership_name;?></div>
                                                          <div name="dealership-address" id="dealership-address" class=" padding-bottom-15 display-inline-block col-md-12 text-center inline-text-strong "><?php echo $q->dealer_address;?></div>
                                                          
                                                          <input type="hidden" name="dealership-name" id="dealership-name" value="<?php echo $q->dealership_name;?>" />
                                                          <input type="hidden" name="dealership-address" id="dealership-address" value="<?php echo $q->dealership_name;?>" />
                                                          <input type="hidden" name="dealer-email" id="dealer-email" value="<?php echo  $q->email; ?>" />
                                                          <input type="hidden" name="vehicle-quote-id" id="vehicle-quote-id" value="<?php echo  $q->quote_id; ?>" />
                                                          <input type="hidden" name="datetime" id="datetime" value="<?php echo $box_day.", ".$box_date." @ " .$box_time." MST";?>" />
                                                          <input type="hidden" name="dealer-name" id="dealer-nam" value="<?php echo $q->firstname;?>" />
                                                          
                                                          <!-- The erroroverlay -->
                                                          <div id="error-overlay-id" class="error-overlay">
                                                            <!-- error-overlay content -->
                                                            <div class="error-overlay-content">
                                                              <span class="close-error">&times;</span>
                                                              <p class="text-center">Selected date is in the past!</p>
                                                            </div>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    </div>
                                                    <div class="col-xl-12">
                                                      <div class="float-left"><a class="button gray mfp-close">Cancel</a></div>
                                                      <div class="float-right"><input type="submit" value="Schedule Appointment" class="button ripple-effect" /></div>
                                          
                                                    </div>

                                                  </div>
                                                </div>
                                              </div>

                                              
                                            </form>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    </div>
                                </div>
                                <!-- Schedule Appointment / End -->
                              </div>
                            </div>

                          </div>

                        </div>
                      </div>

                      <!-- View Vehicle Details Popup ================================================== -->
                        <div id="small-dialog-<?php echo  $q->id; ?>" class="small-dialog zoom-anim-dialog mfp-hide" style="max-width:850px;">
                          <!--Tabs -->
                          <div class="sign-in-form">
                            <ul class="popup-tabs-nav">
                              <li><a href="#tab">Vehicle Details</a></li>
                            </ul>
                            <div class="popup-tabs-container">
                              <div class="popup-tab-content" id="tab">
                                <div id="vehicle-content-<?php echo  $q->id; ?>">
                                  <h3><?php echo $q->make ?> <?php echo $q->model ?> <?php echo $q->year ?></h3>
                                  <div class="float-left margin-top-40 job-listing">
                                    <img width="400" height="300" src="<?php echo $q->image_url?>" alt="">
                                  </div>
                                  <div class="float-right">
                                  <table id="dialog-table" class="basic-table ">
                                    <tr><td>Make: <?php echo $q->make ?></td></tr>
                                    <tr><td>Model: <?php echo $q->model ?></td></tr>
                                    <tr><td>Year: <?php echo $q->year ?></td></tr>
                                    <tr><td>Trim: <?php echo $q->trim ?></td></tr>
                                    <tr><td>Body: <?php echo $q->body ?></td></tr>
                                    <tr><td>Mileage: <?php echo $q->mileage ?></td></tr>
                                    <tr><td>Exterior Color: <?php echo $q->exterior_color ?></td></tr>
                                    <tr><td>Seats: <?php echo $q->seats ?></td></tr>
                                    <tr><td>Engine: <?php echo $q->engine ?></td></tr>
                                    <tr><td>Drive: <?php echo $q->drive ?></td></tr>
                                    <tr><td>Transmission: <?php echo $q->transmission ?></td></tr>
                                    <tr><td>Fuel: <?php echo $q->fuel ?></td></tr>
                                  </table> 
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- View Vehicle Details Popup / End -->

                    </div> 

                
                  <?php endforeach?>        

                  <?php }else{ ?>
                    <?php if($value === false){ ?>
                    <!--- No Quotes---->
                    <div class="margin-bottom-70">
                      <div class="col-xl-12 col-lg-12 no-quote">
                        <i class="icon-feather-radio font-100"></i>
                        <p class="font-25"><strong>It Looks Like There Are Not Many Dealers In The Area You Specified.</strong></p>
                        <p class="font-18">We recommend that you expand your area in order to draw more attention.</p>                        
                      </div>
                      <br>
                     
                      <form method="post" id="vehicle-form-final" action="<?php echo base_url("customer/update_search/$quote"); ?>">
                          <div class="container col-xl-8">
                           <div class="content">
                              <div class="row">
                                
                                <div class="col-xl-4">
                                  <div class="submit-field">
                                    <span>Dealers within </span>
                                    <select class="selectpicker with-border" id="vehicle_miles" name="miles" data-size="3" title="Select One">
                                    <option value="10" <?php echo ($nomatch[$quote]['miles'] == 10) ? "selected" : "" ?>>10</option>
                                    <option value="25" <?php echo ($nomatch[$quote]['miles'] == 25) ? "selected" : "" ?>>25</option> 
                                    <option value="50" <?php echo ($nomatch[$quote]['miles'] == 50) ? "selected" : "" ?>>50</option> 
                                    <option value="100" <?php echo ($nomatch[$quote]['miles'] == 100) ? "selected" : "" ?>>100</option>
                                    <option value="150" <?php echo ($nomatch[$quote]['miles'] == 150) ? "selected" : "" ?>>150</option>
                                    <option value="200" <?php echo ($nomatch[$quote]['miles'] == 200) ? "selected" : "" ?>>200</option>
                                    <option value="10000" <?php echo ($nomatch[$quote]['miles'] == 10000) ? "selected" : "" ?>>200+</option> 
                                    </select>
                                  </div>
                                </div>
                  
                                <div class="col-xl-4">
                                  <div class="submit-field">									
                                  <span>miles from</span>
                                  <input type="text" class="input-text with-border" name="zipcode" id="zipcode" value="<?php echo $nomatch[$quote]['zipcode']; ?>" required/>
                                  </div>
                                </div>

                                <!-- Button -->
                                <div class="col-xl-4 margin-top-30 margin-bottom-30">
                                  <button class="button  ripple-effect" type="submit" form="vehicle-form-final">Update my search</button>
                                </div>

                              </div>
                            </div>
                          </div>

                      </form>
                    </div>
                    <!--- end No Quotes---->
                    <?php }else{ ?>
                    <!--- No Quotes---->
                    <div>
                      <div class="col-xl-12 col-lg-12 no-quote"><i class="icon-line-awesome-car font-100"></i>
                        <p class="font-25"><strong>Dealers are currently working on a quote for you!</strong></p>
                        <p class="font-18">We will notify you once a quote is submitted.</p>
                      </div>
                      
                    </div>
                    <!--- end No Quotes---->
                  <?php } ?>
                <?php } ?>
              </div>             
            <?php endforeach ?>            
          </div> 

        </div>

      </div>
			<!-- Tabs Container / End -->       	
 				 
    </div> 
  </div>
</div>



