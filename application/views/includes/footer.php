
</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/jquery-migrate-3.0.0.min.js'); ?>"></script> 
<script src="<?php echo base_url('assets/js/mmenu.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/tippy.all.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/simplebar.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-slider.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap-select.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/snackbar.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/clipboard.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/counterup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/magnific-popup.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/slick.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/js/custom.js'); ?>"></script>
  
<!-- Snackbar // documentation: https://www.polonel.com/snackbar/ -->
<script>
// Snackbar for user status switcher
$('#snackbar-user-status label').click(function() { 
	Snackbar.show({
		text: 'Your status has been changed!',
		pos: 'bottom-center',
		showAction: false,
		actionText: "Dismiss",
		duration: 3000,
		textColor: '#fff',
		backgroundColor: '#383838'
	}); 
}); 

var initialLoad = true; 
$(document).ready(function(){   
	base_url = "<?php echo base_url(); ?>";
	var user_to = $('#chatbox').attr("data-id"); 
	
	
	activeTab();

	//registration select
	$('#vehicle_make').change(function()
	{
		
		var make = $(this).val(); 
		
		$.ajax({
			type: "POST",
			url: base_url + "vehicle/get_vehicle_model",
			data: {make},
			cache: false,
			success: function(models)
			{
				$models = JSON.parse(models);
				
				html = ""; 
				$.each($models, function( index, value ) { 
				 
						html += '<option value="'+value.model+'">'+value.model+'</option>'; 
					
				});
				$('#vehicle_model').html(html);
				$('#vehicle_model').selectpicker('refresh'); 
			} 
		});
	
	});

	$('#trade_make').change(function()
	{
		
		var make = $(this).val(); 
		
		$.ajax({
			type: "POST",
			url: base_url + "vehicle/get_vehicle_model",
			data: {make},
			cache: false,
			success: function(models)
			{
				$models = JSON.parse(models);
				
				html = ""; 
				$.each($models, function( index, value ) { 
				 
						html += '<option value="'+value.model+'">'+value.model+'</option>'; 
					
				});
				$('#trade_model').html(html);
				$('#trade_model').selectpicker('refresh'); 
			} 
		});
	
	});

	$('#vehicle_model').change(function()
	{
		
		var model = $(this).val(); 
		
		$.ajax({
			type: "POST",
			url: base_url + "vehicle/get_vehicle_trim",
			data: {model},
			cache: false,
			success: function(trims)
			{
				$trims = JSON.parse(trims);
				
				html = ""; 
				$.each($trims, function( index, value ) { 
				 
						html += '<option value="'+value.trim+'">'+value.trim+'</option>'; 
					
				});
				$('#vehicle_trim').html(html);
				$('#vehicle_trim').selectpicker('refresh'); 
			} 
		});
	
	});


	$('#trade_model').change(function()
	{
		
		var model = $(this).val(); 
		
		$.ajax({
			type: "POST",
			url: base_url + "vehicle/get_vehicle_trim",
			data: {model},
			cache: false,
			success: function(trims)
			{
				$trims = JSON.parse(trims);
				
				html = ""; 
				$.each($trims, function( index, value ) { 
				 
						html += '<option value="'+value.trim+'">'+value.trim+'</option>'; 
					
				});
				$('#trade_trim').html(html);
				$('#trade_trim').selectpicker('refresh'); 
			} 
		});
	
	});


	$('#zipcode').on('keyup', function(){
		var zipcode = $(this).val();
		if(zipcode != "" && $.isNumeric(zipcode)) {
			html = '<option value="0">Exact</option>'; 
			html += '<option value="10">10</option>'; 
			html += '<option value="25">25</option>'; 
			html += '<option value="50">50</option>'; 
			html += '<option value="100">100</option>'; 
			html += '<option value="150">150</option>'; 
			html += '<option value="200">200</option>'; 
			html += '<option value="10000">200+</option>'; 

			$('#vehicle_miles').html(html);
			$('#vehicle_miles').selectpicker('refresh'); 
		}
	});

	$('#dealer-vehicles').change(function()
	{		
		
		var id = $(this).val(); 
		var selected_vehicle_dtl = document.getElementById('vehicle-details'); 
		var vehicle_picker = document.getElementById('vehicle-selection'); 
		var add_quote = document.getElementById('add-quote'); 
		var offer = document.getElementById('offer');  
	    selected_vehicle_dtl.style.display = '';   
	    add_quote.style.display = '';      
	    offer.style.display = '';
		vehicle_picker.style.display = 'none';  

		console.log(id);

		$.ajax({
			type: "POST",
			url: base_url + "vehicle/get_vehicle_by_id",
			data: {id},
			cache: false,
			success: function(vehicles)
			{
				$vehicle_dtl = JSON.parse(vehicles);				 
				$.each($vehicle_dtl, function( index, value ) { 
				 
					html = '<tr>';
					html += '<td data-label="Column 1">Make</td>';
					html += '<td data-label="Column 2">'+value.make+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">Model</td>';
					html += '<td data-label="Column 2">'+value.model+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">Year Range</td>';
					html += '<td data-label="Column 2">'+value.year+'</td> ';
					html += '</tr>';  
					html += '<tr>';
					html += '<td data-label="Column 1">Mileage Range</td>';
					html += '<td data-label="Column 2">'+value.mileage.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">MSRP</td>';
					html += '<td data-label="Column 2">$'+value.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">Exterior Color</td>';
					html += '<td data-label="Column 2">'+value.exterior_color+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">Interior Color</td>';
					html += '<td data-label="Column 2">'+value.interior_color+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">Transmission</td>';
					html += '<td data-label="Column 2">'+value.transmission+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<tr>';
					html += '<td data-label="Column 1">Fuel Type </td>';
					html += '<td data-label="Column 2">'+value.fuel+'</td> ';
					html += '</tr>'; 
					html += '<tr>';
					html += '<td data-label="Column 1">Drive Type</td>';
					html += '<td data-label="Column 2">'+value.drive+'</td> ';
					html += '</tr>';  
				});
				$('#vehicle-details').html(html); 
			} 
		});
  
	});

	$('#date').on('change', function(){

		var now = new Date();  
		var date = new Date($(this).val());		 

		var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
		var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    var appointment_datetime = document.getElementById("datetime").innerHTML;
		var appointment_time = appointment_datetime.slice(appointment_datetime.indexOf("@")+1,appointment_datetime.length);
	
		var appointment_date = days[date.getDay()]+", "+months[date.getMonth()]+" "+ date.getDate() + nth(date.getDate())+" "+date.getFullYear() + " @ ";
	

		if (date < now) {
			// Get the erroroverlay
			var erroroverlay = document.getElementById('error-overlay-id');
			erroroverlay.style.display = "block"; 

			// Get the <span> element that closes the erroroverlay
			var span = document.getElementsByClassName("close-error")[0]; 

			// When the user clicks on <span> (x), close the erroroverlay
			span.onclick = function() {
				erroroverlay.style.display = "none";
			}

			// When the user clicks anywhere outside of the erroroverlay, close it
			window.onclick = function(event) {
				if (event.target == erroroverlay) {
					erroroverlay.style.display = "none";
				}
			}
			var day = ("0" + now.getDate()).slice(-2);
			var month = ("0" + (now.getMonth() + 1)).slice(-2);

			var today = now.getFullYear()+"-"+(month)+"-"+(day) ;

			$(this).val(today);  

			console.log($(this).val());
			appointment_date = days[now.getDay()]+", "+months[now.getMonth()]+" "+ now.getDate() + nth(now.getDate())+" "+now.getFullYear() + " @ ";
	
		}   	 

		document.getElementById("datetime").innerHTML = appointment_date + appointment_time ; 
	});
 

	$('#time').on('change', function(){
		
		var time = $(this).val();
		var time = tConvert (time); 

		var appointment_datetime = document.getElementById("datetime").innerHTML;
		var appointment_date = appointment_datetime.slice(0,appointment_datetime.indexOf("@"));
		document.getElementById("datetime").innerHTML = appointment_date + " @ " + time + " MST";		 

	});

	//hide success notification
	$(".request-success .notification").fadeOut(5000);

});

function nth(date) {
  if (date > 3 && date < 21) return 'th'; 
  switch (date % 10) {
    case 1:  return "st";
    case 2:  return "nd";
    case 3:  return "rd";
    default: return "th";
  }
}

function tConvert (time) {
  // Check correct time format and split into components
  var time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
  console.log(time); 
  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value 
    time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM 
    time[0] = +time[0] % 12 || 12; // Adjust hours 
  }
  return time.join (''); // return adjusted time or original string
}

function loadMessage(id, user_initials){
	 
	var user_to = id;
	var initials = user_initials; console.log(initials);
	$.ajax({
		type: "POST",
		url: base_url + "messages/get_message_thread", 
		data: {user_to}, 
		cache:false,
		success: 
			function(data){   
				//add html 
				$msgs = JSON.parse(data);
				console.log($msgs);
				html = ""; 
				$.each($msgs, function( index, value ) {
					
					if(user_to == value.user_from){
						html += '<div class="message-bubble">';
						html += '<div class="message-bubble-inner">';
						html += '  <div class="message-avatar"><p class="user-to">' + user_initials[user_to] + '</p></div>';
						html += '  <div class="message-text"><p>' + value.content + '</p></div>';
						html += '</div>';
						html += '<div class="clearfix"></div>';
						html += '</div>';
					}
					
					if(user_to == value.user_to){
						html += '<div class="message-bubble me">';
						html += '<div class="message-bubble-inner">';
						html += '  <div class="message-avatar"><p class="user-from">' + user_initials[value.user_from] + '</p></div>';
						html += '  <div class="message-text"><p>' + value.content + '</p></div>';
						html += '</div>';
						html += '<div class="clearfix"></div>';
						html += '</div>';
					}
					
				});
				
				$('#message-content-'+id).html(html);
			}
		});// you have missed this bracket
	return false;
}

function sendMessage(id, user_from, user_initials){
	var user_to = id;
	$.ajax({
		type: "POST",
		url: base_url + "messages/send_message", 
		data: {textbox: $("#textbox-"+id).val(), user_to: user_to},
		dataType: "text",   
		success: 
			function(data){ console.log(data);
				//add html 
				html = '<div class="message-bubble me">';
				html += '<div class="message-bubble-inner">';
				html += '  <div class="message-avatar"><p class="user-from">' + user_initials[user_from] + '</p></div>';
				html += '  <div class="message-text"><p>' + $("#textbox-"+id).val() + '</p></div>';
				html += '</div>';
				html += '<div class="clearfix"></div>';
				html += '</div>';

				$('#message-content-'+id).append(html);
				$("#textbox-"+id).val("");
			}
		});// you have missed this bracket
	return false;
}

function viewTradeIn(){
	$('#trade-in').removeClass('hidden');
}
 
// Change Tab
function activeTab() {
	var getTabId = $('.new-request').data("req-id"); 
	
	if(getTabId != ""){
		$('.tabs-header li').removeClass('active');
		// Add Class
		$('li.tab-'+getTabId).addClass('active');

		$('#tabcontent .tab').removeClass('active');
		$('div[data-tab-id=' + getTabId + ']').addClass('active');
	}
		 
}
 

</script>


<!-- Google Autocomplete -->
<script>
	function initAutocomplete() {
		 var options = {
		  types: ['(cities)'],
		  // componentRestrictions: {country: "us"}
		 };

		 var input = document.getElementById('autocomplete-input');
		 var autocomplete = new google.maps.places.Autocomplete(input, options);
	}

	// Autocomplete adjustment for homepage
	if ($('.intro-banner-search-form')[0]) {
	    setTimeout(function(){ 
	        $(".pac-container").prependTo(".intro-search-field.with-autocomplete");
	    }, 300);
	}

</script>

<!-- Google API & Maps -->
<!-- Geting an API Key: https://developers.google.com/maps/documentation/javascript/get-api-key -->
<script src="https://maps.googleapis.com/maps/api/js?key=&libraries=places"></script>

</body>
</html>