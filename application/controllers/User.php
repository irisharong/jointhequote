<?php 
 require 'vendor/autoload.php';

class User extends CI_Controller{
    function __construct()
    {
        parent::__construct(); 
        $this->load->library('form_validation'); 
        
        $this->load->library('session'); 
        $this->load->model('Login_model'); 
        $this->load->model('Register_model');
        $this->load->model('User_model');
        $this->load->model('Vehicle_model');
               
        $this->load->library('form_validation'); 
    } 

    /*
     * Listing of customer_user
     */
    function index()
    {
        $data['customer_user'] = $this->User_model->get_all_customer_user();
        
        $data['_view'] = 'customer_user/index';
        $this->load->view('layouts/main',$data);
    }

    function login(){  
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == FALSE)
		{ 
			$this->load->view('includes/header');
            $this->load->view('user/login');
			$this->load->view('includes/footer');
		}
		else
		{  
            $email = $this->input->post('email');
            $password = $this->input->post('password');

			$this->load->model('login_model');
			$verify = $this->login_model->verify( $email, $password);
			//var_dump($verify);die();
            if( $verify == "dealer"){
                redirect(base_url('dealer/team'));
            }elseif($verify == "salesrep"){
                redirect(base_url('dealer/request'));
            }else{ //user
                redirect(base_url('customer'));
            }                 
		}
        
    }

    function register(){
         
		$this->form_validation->set_rules('firstname', 'First Name', 'trim|required');
		$this->form_validation->set_rules('lastname', 'Last Name', 'trim|required');  
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|callback_check_email|required|valid_email');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|md5'); 
        if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('includes/header');
            $this->load->view('user/registration');
			$this->load->view('includes/footer');
		}
		else
		{
            $user_id = $this->Register_model->add_user();

            //email
            $data['firstname'] = $this->input->post('firstname');
            $data['lastname'] = $this->input->post('lastname');
            $data['email'] = $this->input->post('email');
            $this->send_email($data);
            redirect("user/request/$user_id");
		} 
    }

    function request($user_id){ 
       

        if(isset($_POST) && count($_POST) > 0)     
        {    

            $request_end_time = $this->get_request_end_time();
            $vehicle_status = implode(",",$this->input->post('vehicle_status'));
             $data = array(
                'user_id' => $user_id,
                'make' => $this->input->post('vehicle_make'),
                'model' => $this->input->post('vehicle_model'),
                'trim' => $this->input->post('vehicle_trim'),
                'year_from' => $this->input->post('year_from'),
                'year_to' => $this->input->post('year_to'), 
                'price_from' => $this->input->post('price_from'),
                'price_to' => $this->input->post('price_to'), 
                'vehicle_status' => $vehicle_status,
                'trade_in' => $this->input->post('trade_in'), 
                'requested_time' => date('Y-m-d G:i:s'), 
                'request_end_time' => $request_end_time,
                'status' => 0,
            );
                      
            $customer_request_id = $this->Register_model->add_customer_request($data);

            
            if( $customer_request_id && $this->input->post('trade_in') == 1){
                
                $trade = array(
                    'customer_request_id' => $customer_request_id,
                    'make' => $this->input->post('trade_make'),
                    'model' => $this->input->post('trade_model'),
                    'trim' => $this->input->post('trade_trim'),
                    'year' => $this->input->post('trade_year'), 
                    'price' => $this->input->post('trade_price'), 
                    'internal' => $this->input->post('trade_internal'), 
                    'external' => $this->input->post('trade_external'), 
                );
                $trade_id = $this->Register_model->add_trade_request($trade);
            } 
            redirect("user/request_last/$user_id/$customer_request_id");
        }else{
 
            $data['user_id'] = $user_id;
            $data['make'] = $this->Vehicle_model->get_all_make();            
            $data['year'] = $this->Vehicle_model->get_all_year();
            $this->load->view('includes/header');
            $this->load->view('user/vehicle_request', $data);
			$this->load->view('includes/footer');
        }
    }

    function request_last($user_id, $customer_request_id){
        if(isset($_POST) && count($_POST) > 0)     
        {     
            $data = array(  
				'zip_code' => $this->input->post('zipcode'),
				'miles' => $this->input->post('miles'), 
				'status' => 1,
            );
            
            $this->Register_model->update_customer_request_byId($data, $customer_request_id);
           //$customer_request_id = $this->Register_model->update_customer_request($data, $user_id);
            
            redirect("customer/index/$customer_request_id");
        }else{
            $data['user_id'] = $user_id;
            $data['cr_user_id'] = $customer_request_id;
            $this->load->view('includes/header');
            $this->load->view('user/vehicle_request_final', $data);
			$this->load->view('includes/footer');
        }
    }
 
    function check_email($str){
        $register = $this->Register_model;
        $check_email = $register->check_email($str);
        if($check_email){
            $this->form_validation->set_message('check_email', 'Email ('.$str.') already exist.');
            return false;   
        }else{
            return true;   
        }
    }

    /*
     * Deleting customer_user
     */
    function remove($id)
    {
        $customer_user = $this->User_model->get_customer_user($id);

        // check if the customer_user exists before trying to delete it
        if(isset($customer_user['id']))
        {
            $this->User_model->delete_customer_user($id);
            redirect('customer_user/index');
        }
        else
            show_error('The customer_user you are trying to delete does not exist.');
    }

    function get_request_end_time(){

        $default_start_time = date('Y-m-d G:i:s', strtotime('9:00 AM'));
        $default_end_time = date('Y-m-d G:i:s', strtotime('6:00 PM'));
        $requested_time = date('Y-m-d G:i:s');

        $weekday= date("l", strtotime($requested_time));
        $weekday = strtolower($weekday); 

        if($weekday == "saturday" || $weekday == "sunday"){
            if ($weekday == "saturday") {
                $monday = date ('Y-m-d G:i:s',strtotime('+2 Day 9 AM'));
                $request_end_time = date ('Y-m-d G:i:s',strtotime('+6 hour', strtotime($monday)));
            } else if ($weekday == "sunday"){
                $monday = date ('Y-m-d G:i:s',strtotime('+1 Day 9 AM'));
                $request_end_time = date ('Y-m-d G:i:s',strtotime('+6 hour', strtotime($monday)));
            }
        }else{
            if($requested_time < $default_start_time){
                //9am + 6hrs sameday
                $request_end_time = date('Y-m-d G:i:s', strtotime('+6 hour', strtotime($default_start_time)));
            }else if($requested_time > $default_end_time){
                //9am + 6hr the next day
                $tomorrow = date ('Y-m-d G:i:s',strtotime('+1 Day 9 AM'));
                $request_end_time = date('Y-m-d G:i:s', strtotime('+6 hour', strtotime($tomorrow)));
            }else if($requested_time > $default_start_time && $requested_time < $default_end_time){
                $request_end_time = date ('Y-m-d G:i:s',strtotime('+6 hour', strtotime($requested_time)));

                //if end time is greater than 6PM, calculate difference then add the difference the next day after 9am
                if(strtotime($request_end_time) > strtotime($default_end_time)){
                    $diff = strtotime($request_end_time) - strtotime($default_end_time);
                    $tomorrow = date ('Y-m-d G:i:s',strtotime('+1 Day 9 AM'));
                    
                    $request_end_time = strtotime($tomorrow) + $diff;
                    $request_end_time = date ('Y-m-d G:i:s', $request_end_time);
                }                   
            }
        }

        return $request_end_time;
    }
    
    function logout(){
        $this->session->sess_destroy();
		redirect('login');
    }   

    function rolekey_exists($key) {
        $this->load->model('User_model');
        $this->User_model->mail_exists($key);
    }

    function reset(){
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('email', 'Email', 'callback_rolekey_exists');
        if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('includes/header');
            $this->load->view('user/forgot_password');
			$this->load->view('includes/footer');
		}
		else
		{
            $email = $this->input->post('email'); 
            $user = $this->User_model->get_user_by_email($email);
 
            foreach($user as $info){
                $firstname = $info->firstname;
                $lastname = $info->lastname;
            }

            $email_encode = base64_encode ($email);

            $base_url = base_url("user/reset_pass/$email_encode");
            $message_header = "Hi ".$firstname." ".$lastname.",";  
            $message_body = "Reset your password, and we'll get you on your way. <br \><br \>";
            $message_body .= "To change your password, click <a href='$base_url'>here</a>.";
            
            $email_to = $email;
            $email_data = array(
                'first_name' 		=> $firstname,
                'last_name' 		=> $lastname, 
                'email' 			=> $email_to, 
                'message_header' 	=> $message_header,
                'message_body'      => $message_body,
            );  
            
            $message_template = $this->load->view('user/forgot_password_email',$email_data,TRUE);
            $subject = "$firstname, here's the link to reset your password";
          
            $send = $this->send_emails($message_template, $subject, $email_to);

            $data['sent'] = $send;
            $this->load->view('includes/header');
            $this->load->view('user/forgot_password', $data);
			$this->load->view('includes/footer');
		}
    }

    function reset_pass($email_encode = false){
        $email = base64_decode ($email_encode);
        
        $this->form_validation->set_rules('new_password', 'Password', 'trim|required|md5'); 
        $this->form_validation->set_rules('re_password', 'Password', 'trim|required|md5'); 

        if($this->input->post()){   
			$new_password = $this->input->post('new_password');
            $re_password = $this->input->post('re_password');	 
            
            if ($this->form_validation->run() == FALSE)
            {
            	$data['email_encode'] = $email_encode;		
                $this->load->view('includes/header');
                $this->load->view('user/update_password', $data);
                $this->load->view('includes/footer'); 
            }
            else
            {
                if($new_password != $re_password){
                    $data['error'] = true;				
                }else{
                    
                    $result = $this->User_model->update_password($email, $new_password);
                    
                    if($result){
                        $data['success'] = true;
                    }else{
                        $data['error2'] = true;
                    }
                } 
            }

		}
		$data['email_encode'] = $email_encode;		
        $this->load->view('includes/header');
		$this->load->view('user/update_password', $data);
		$this->load->view('includes/footer'); 
    }

    function send_emails($message_template, $subject, $email_to){
          
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("jointhequote@jointhequote.com", "Join the Quote");
        $email->setSubject($subject);
        $email->setReplyTo('jointhequote@jointhequote.com');
        $email->addTo("$email_to", "Admin User");
        $email->addContent(
            "text/html", "$message_template"
        );

        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $response_code = $response->statusCode();             

        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }

        //var_dump("<pre>", $response); die();  
        return $response_code;     

    }

    function send_email($data)
    {         

        $message_header = "Welcome to Join The Quote, ".$data['firstname'];  
        $email_to = $data['email'];
		$email_data = array(
			'first_name' 		=> $data['firstname'],
			'last_name' 		=> $data['lastname'], 
			'email' 			=> $email_to, 
			'message_header' 	=> $message_header,
        );  
		
		$message = $this->load->view('user/welcome_email',$email_data,TRUE);
		
        $email = new \SendGrid\Mail\Mail(); 
        $email->setFrom("jointhequote@jointhequote.com", "Join the Quote");
        $email->setSubject("Welcome to Join The Quote, ".$data['firstname']);
        $email->setReplyTo('jointhequote@jointhequote.com');
        $email->addTo("$email_to", "Admin User");
        $email->addContent(
            "text/html", "$message"
        );

        $sendgrid = new \SendGrid('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');
        try {
            $response = $sendgrid->send($email);
            $response_code = $response->statusCode();             

        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }

        //var_dump("<pre>", $response); die();  
        return $response_code;           
        

    }
}
