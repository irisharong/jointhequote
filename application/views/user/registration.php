<div class="dashboard-content-container" data-simplebar>
		<div class="dashboard-content-inner" >
			
			
			<!-- Row -->
			<div class="row">

				<!-- Dashboard Box -->
				<div class="container col-xl-4 margin-bottom-30">
					<div class="dashboard-box margin-top-0">
 
            <div class="welcome-text with-padding padding-top-30">
              <h3>Create Account</h3>              
            </div> 

            <?php echo validation_errors('<div class="alert alert-danger alert-dismissable">', '</div>'); ?>

            <!-- Form -->
            <form method="post" id="register-form" action="<?php echo base_url('user/register'); ?>">
            <div class="content with-padding padding-bottom-10">
              <div class="row">

                <div class="col-xl-6">
                  <div class="submit-field">
                    <input type="text" class="input-text with-border" name="firstname" id="firstname" placeholder="First Name" required/>
                  </div>
                </div>

                <div class="col-xl-6">
                  <div class="submit-field">
                    <input type="text" class="input-text with-border" name="lastname" id="lastname" placeholder="Last Name" required/>
                  </div>
                </div>
                
                <div class="col-xl-12">
                  <div class="submit-field">
                    <input type="text" class="input-text with-border" name="email" id="email" placeholder="Email" required/>
                  </div>
                </div>

                <div class="col-xl-12">
                  <div class="submit-field">
                    <input type="text" class="input-text with-border" name="phone" id="phone" placeholder="Phone" required/>
                  </div>
                </div>

                <div class="col-xl-12">
                  <div class="submit-field">
                    <input type="password" class="input-text with-border" name="password" id="password" placeholder="Password" required/>
                  </div>
                </div>

                <!-- Button -->
                <button class="button full-width button-sliding-icon ripple-effect" type="submit" form="register-form">Sign Up <i class="icon-material-outline-arrow-right-alt"></i></button>
                                
                <div class="container col-xl-12 padding-top-10">
                  <div class="submit-field center">
                    <span class="welcome-text">Your information is secure and is not shared unless you request to come in contact with a dealership.</span>
                  </div>
                </div>
                
                <div class="container col-xl-12">
                  <div class="submit-field welcome-text">
                    <a href="<?php echo base_url('user/login'); ?>">Already a Member? Login</a>
                  </div>
                </div>

              </div>
            </div>
            </form>

          </div>
				</div>
 
			</div>
			<!-- Row / End -->
 
		</div>
	</div>